import algorithms.HungarianAlgorithm
import org.scalatest.featurespec.AnyFeatureSpec
import org.scalatest.matchers.must.Matchers.have
import org.scalatest.matchers.should.Matchers.{should, the}
import tsp.{AdjMatrix, LocalSearch, Main, TSP}

/**
  * Created by velizar on 11/07/16.
  */
class TSPTest extends AnyFeatureSpec {
  Feature("testParseConstraint") {
    val parseConstraint = Main.parseConstraint _

    Scenario("No constraints") {
      assert((None, None) == parseConstraint("-,-", 1.toByte))
    }

    Scenario("Two constraints") {
      val outcome = parseConstraint("234,567", 2.toByte)
      assert((Some(2, 567), Some(2, 234)) == outcome)
    }

    Scenario("LT constraint only") {
      val outcome = parseConstraint("-,20", 3.toByte)
      assert((Some(3, 20), None) == outcome)
    }

    Scenario("GT constraint only") {
      val outcome = parseConstraint("12015,-", 4.toByte)
      assert((None, Some(4, 12015)) == outcome)
    }
  }


  // One big test covering all common concerns
  Feature("testParseConstraints") {
    Scenario("Case 1") {
      val args: Array[String] = Array(
        "_/-,-",
        "_/-,21",
        "_/9,-",
        "_/26,32",
        "_/20,947")

      val (ltActual, gtActual) = Main.parseConstraints(args)

      assert(Map(1 -> 21, 3 -> 32, 4 -> 947) == ltActual)
      assert(Map(2 -> 9, 3 -> 26, 4 -> 20) == gtActual)
    }
  }

  Feature("testMain") {
    Scenario("Solution is obviously 0, 1, 3, 2, 4 or 0, 4, 2, 3, 1") {
      val args: Array[String] = Array("exact",
        "0,10,17,1000,10/-,-",
        "10,0,1000,10,990/-,101",
        "17,1000,0,10,10/9,-",
        "1000,10,10,0,1000/6,32",
        "10,990,10,1000,0/2,947")

      val stream = new java.io.ByteArrayOutputStream()
      Console.withOut(stream) {
        Main.main(args)
      }
      assert(Set("0,1,3,2,4", "0,4,2,3,1") contains stream.toString)
    }

    Scenario("Same result when a wait is needed") {
      val args: Array[String] = Array("exact",
        "0,10,17,1000,10/-,-",
        "10,0,1000,10,990/-,101",
        "17,1000,0,10,10/25,-",    // notice 25 constraint
        "1000,10,10,0,1000/25,62", // notice 25 constraint
        "10,990,10,1000,0/2,947")

      val stream = new java.io.ByteArrayOutputStream()
      Console.withOut(stream) {
        Main.main(args)
      }
      assert(Set("0,1,3,2,4", "0,4,2,3,1") contains stream.toString)
    }

    Scenario("Changes route to satisfy LT constraint") {
      val args: Array[String] = Array("exact",
        "0,10,17,1000,10/-,-",
        "10,0,1000,10,990/-,3101",
        "17,1000,0,10,10/3,19",     // notice 19 constraint
        "1000,10,10,0,1000/-,3392",
        "10,990,10,1000,0/2,3947")

      val stream = new java.io.ByteArrayOutputStream()
      Console.withOut(stream) {
        Main.main(args)
      }
      assert("0,2,3,1,4" == stream.toString)
    }
  }

  Feature("Hungarian Method") {
    Scenario("Dataset 1 (valid TSP)") {
      val adj: Array[Array[Double]] = Array(
        Array(99,  2,  17, 1000, 11),
        Array(50, 99,  90,    3, 11),
        Array(60, 10,  99,   53,  4),
        Array(90, 10,   5, 9999, 17),
        Array( 7, 10,  17, 1000, 99)).map(_.map(_.toDouble))
      val expected = Array(1, 3, 4, 2, 0)

      val res = new HungarianAlgorithm(adj).execute()
      assert(res.toList == expected.toList)
    }

    Scenario("Dataset 2 (two TSP subtours)") {
      val adj: Array[Array[Double]] = Array(
        Array(99,  2,  17, 1000, 11),
        Array( 3, 99,  90,   11, 16),
        Array(60, 10,  99,    4, 11),
        Array(90, 10,  12, 9999,  5),
        Array(55, 10,   2, 1000, 99)).map(_.map(_.toDouble))
      val expected = Array(1, 0, 3, 4, 2)

      val res = new HungarianAlgorithm(adj).execute()
      assert(res.toList == expected.toList)
    }
  }

  Feature("With unsatisfiable constraints") {
    val cliInput = Vector(
      "0,10,1000,1000,10/-,-",
      "10,0,1000,10,990/-,3101",
      "17,5,1000,10,10000/3,50", // the 50 contraint is unsatisfiable
      "10,5,1000,0,1000/-,3392",
      "10,5,1000,1000,0/2,3947")
    val adj = Main.cliArgsToAdjMatrix(cliInput)

    Scenario("Main method returns the right error message with exact") {
      assert(Main.runFromCL(("exact" +: cliInput).toArray) == "Error: unsatisfiable constraints")
    }
    Scenario("Main method returns the right error message with approximate") {
      assert(Main.runFromCL(("approximate" +: cliInput).toArray) == "Error: unsatisfiable constraints")
    }
    Scenario("Dynamic programming solver throws the right exception") {
      the [IllegalArgumentException] thrownBy TSP.solveTspDynamic(adj) should have message "Unsatisfiable constraints"
    }
    Scenario("Branch and bound solver throws the right exception") {
      the [IllegalArgumentException] thrownBy TSP.solveTspBranchAndBound(adj) should have message "Unsatisfiable constraints"
    }
    Scenario("Local search solver throws the right exception") {
      the [IllegalArgumentException] thrownBy LocalSearch.quickSearch(adj) should have message "Unsatisfiable constraints"
    }
  }
}
