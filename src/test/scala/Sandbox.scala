import org.scalatest.featurespec.AnyFeatureSpec
import unused.CarpanetoToth

class Sandbox extends AnyFeatureSpec {
  val run = false
  if (run) {
    Feature("CarpanetoToth") {
      Scenario("Dataset 1") {
        val adjVecFloat = tsp.TSPData.dataset1Float(28)
        val adjFloat = adjVecFloat.map(_.toArray).toArray
        val adj = adjFloat.map(_.map(_.toDouble))

        val solution = CarpanetoToth.solve(adj)
        println((adj.length, solution))
        assert(true)
      }

      Scenario("Dataset 2") {
        val adjVecFloat = tsp.TSPData.dataset2Float(35)
        val adjFloat = adjVecFloat.map(_.toArray).toArray
        val adj = adjFloat.map(_.map(_.toDouble))

        val solution = CarpanetoToth.solve(adj)
        println((adj.length, solution))
        assert(true)
      }

      Scenario("Dataset 3 - real world; asymmetric") {
        val adjInt = tsp.TSPData.dataset3Array(39)
        val adj = adjInt.map(_.map(_.toDouble))

        val solution = CarpanetoToth.solve(adj)
        println((adj.length, solution))
        assert(true)
      }
    }
  }
}
