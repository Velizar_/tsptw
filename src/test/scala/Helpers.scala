import tsp.AdjMatrix

object Helpers {
  val highCost = 1_000_000
  def parseAdjMatrix(in: String): AdjMatrix = {
    val adj = in.replaceAll("-+", highCost.toString).split("\\s+").filter(_.nonEmpty).map(
      line => line.split(",").map(_.toInt).toVector
    ).toVector
    AdjMatrix.from2DVec(adj, Map.empty, Map.empty)
  }

  def prependEntry(adj: AdjMatrix, values: Int): AdjMatrix = {
    val vec = Vector.fill(adj.n + 1)(values) +: adj.twoD.map(values +: _)
    AdjMatrix.from2DVec(vec, adj.lt, adj.mt)
  }
}
