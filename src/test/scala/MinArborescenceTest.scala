import org.scalatest.featurespec.AnyFeatureSpec
import Helpers.{highCost, parseAdjMatrix, prependEntry}
import tsp.{BitSetObject, MinArborescence}

import scala.collection.immutable.NumericRange

class MinArborescenceTest extends AnyFeatureSpec {
  Feature("MinArborescence") {
    Scenario("Trivial tree") {
      val adj = prependEntry(prependEntry(parseAdjMatrix("""
        0000,----,1000
        0010,0000,----
        ----,0100,0000"""), 1), highCost)
      val res = MinArborescence.minArborescence(
        adj,
        BitSetObject.seqToBitSet(Seq(2, 3, 4).map(_.toByte)),
        adj.twoD(1))
      assert(res == 111)
    }

    /**
      * 1. Node 2 goes on the growth path
      * 2. Add 3->2, cost=10
      * 3. Add 2->3, cost=100, they merge
      * 4. Add 4->2, cost=50, replacing 3->2
      * 5. Add 2->4, cost=5
      * 6. Add 1->2, cost=1, replacing 2->3, also causing 3->2 to replace 4->2
      */
    Scenario("With some merging") {
      val adj = prependEntry(parseAdjMatrix("""
        0000,----,0001,----
        ----,0000,0100,0005
        ----,0010,0000,----
        ----,0050,----,0000"""), highCost)
      val res = MinArborescence.minArborescence(
        adj,
        BitSetObject.seqToBitSet(Seq(2, 3, 4).map(_.toByte)),
        adj.twoD(1))
      assert(res == 16)
    }

    /**
      * Add node 2, then 3->2, then 2->3 and merge;
      * Then do the same with 4 and merge, then with 5
      * Finally, the root node points at 2
      */
    Scenario("With a lot of merging in a single node") {
      val adj = prependEntry(parseAdjMatrix("""
        0000,0001,----,----,----
        ----,0000,0100,0005,1000
        ----,0010,0000,----,----
        ----,0050,----,0000,----
        ----,0500,----,----,0000"""), highCost)
      val res = MinArborescence.minArborescence(
        adj,
        BitSetObject.seqToBitSet(Seq(2, 3, 4, 5).map(_.toByte)),
        adj.twoD(1))
      assert(res == 1106)
    }

    // Note: here is a way to introduce a bug while all tests fail:
    //   change SkewHeap so that when it merges, it extracts that.head via pattern-match
    //   then it compares min with that.head rather than with that.min
    //   the result seems to be that the cost is often higher than the min arborescence
    //   this breaks the branch&bound as the heuristic is sometimes costlier than the cheapest TSP
    // It can be worthwhile to write a test which covers that case
  }
}
