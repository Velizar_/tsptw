package unused

import tsp.BitSetObject.{BitSet, bitSetContains, bitSetToQueue, intToBitPos}
import tsp.TSP.NodeID
import tsp.{AdjMatrix, ByteArbrscUnionFind}

/** Minimum spanning arborescence which calculates both the cost and the arborescence
  *
  * When used as a heuristic for branch&bound for TSP, the arborescence may be a tour.
  * It turns out that this is pretty rare: 0.14% for n=18, 0.005% for n=27 for dataset3
  * Also it tends to occur towards the end of the trip, which is less useful because it
  * would prune less.
  *
  * Maybe it can be simplified and sped up by summing the edges to calculate the cost.
  */
object MinArborescenceReturningEdges {
  private var skewHeaps: Array[IMSkewHeap] = _

  def init(adj: AdjMatrix): Unit = {
    given ord: Ordering[(Int, Byte)] = Ordering.by(_._1)
    skewHeaps = Array.fill(adj.n * 2 - 1)(IMSkewHeap.empty)
    for (dst <- 1 until adj.n; src <- 1 until adj.n if src != dst)
      skewHeaps(dst) = skewHeaps(dst).insert((adj(src, dst), src.toByte, dst.toByte))
  }

  var timesATour = 0

  type SuperNodeID = NodeID

  // Note: subgraph never includes 0
  def minArborescence(subgraph: BitSet, edgesFromR: Seq[Int], rId: NodeID): Int = {
    if (subgraph == 0)
      return 0

    val subgraphVec = bitSetToQueue(subgraph).toVector
    val k = subgraphVec.size
    val unionFind = new ByteArbrscUnionFind(subgraphVec.lastOption.getOrElse(-1), k.toByte)
    // a heap of min incoming edges for each supernode
    val inEdges: Array[IMSkewHeap] = skewHeaps.clone
    // keeps track of the cost to insert an edge from the extra root for each supernode
    val edgesFromExtraNode: Array[Int] = edgesFromR.toArray ++ new Array((unionFind.last + k - edgesFromR.size) max 0)
    var totalCost = 0

    // contains the current arborescence in progress
    var growthPath: List[SuperNodeID] = List(subgraphVec.head)

    def root = growthPath.head

    var nodesOnPath: BitSet = intToBitPos(root)

    val parents: Array[List[Int]] = (0 until subgraphVec.lastOption.getOrElse(-1.toByte) + k).map(dst => List(dst)).toArray
    val edgeParents: collection.mutable.Map[(Byte, Byte), Set[Int]] = collection.mutable.Map.empty
    val children: Array[Set[Byte]] = (0 until subgraphVec.lastOption.getOrElse(-1.toByte) + k).map(x => Set(x.toByte)).toArray
    var edgesInOrder: List[(Byte, Byte)] = List.empty
    val extraNodeMapping: collection.mutable.Map[Int, Byte] = collection.mutable.Map.empty

    // each iteration, add the min-cost node connecting to the supernode under growthPath(lastIdx)
    while (!(growthPath.tail == Nil && nodesOnPath == subgraph)) {
      // skip invalid edges
      while (!bitSetContains(subgraph, inEdges(root).min._2) || unionFind.find(inEdges(root).min._2) == unionFind.find(root))
        inEdges(root) = inEdges(root).tail
      val (addedCost, next, dst) = inEdges(root).min
      inEdges(root) = inEdges(root).tail
      inEdges(root) = inEdges(root).addToAll(-addedCost)
      edgesFromExtraNode(root) -= addedCost
      totalCost += addedCost
      edgeParents((next, dst)) = parents(dst).toSet
      edgesInOrder ::= (next, dst)

      if (bitSetContains(nodesOnPath, next)) {
        // we just formed a cycle, merge it into a supernode
        val (nodesBeforeNext, parentOfNext :: newGrowthPath) = growthPath.span(unionFind.find(_) != unionFind.find(next))
        growthPath = newGrowthPath
        val nodesToMerge = parentOfNext :: nodesBeforeNext
        val newSuperNode = unionFind.union(nodesToMerge)
        edgesFromExtraNode(newSuperNode) = Int.MaxValue
        inEdges(newSuperNode) = IMSkewHeap.empty
        for (node <- nodesToMerge) {
          inEdges(newSuperNode) = inEdges(newSuperNode) merge inEdges(node)
          children(newSuperNode) ++= children(node)
          if (edgesFromExtraNode(node) < edgesFromExtraNode(newSuperNode))
            extraNodeMapping(newSuperNode) = extraNodeMapping.getOrElse(node, node)
          edgesFromExtraNode(newSuperNode) = edgesFromExtraNode(newSuperNode) min edgesFromExtraNode(node)
        }
        for (node <- children(newSuperNode))
          parents(node) ::= newSuperNode
        growthPath ::= newSuperNode
      } else {
        nodesOnPath += intToBitPos(next)
        growthPath ::= next
      }
    }

    var edgesInArb: Set[(Byte, Byte)] = Set(rId -> extraNodeMapping.getOrElse(root, root))
    var banListParents: Set[Int] = parents(extraNodeMapping.getOrElse(root, root)).toSet
    for (edge <- edgesInOrder if banListParents.intersect(edgeParents(edge)).isEmpty) {
      edgesInArb += edge
      for (parent <- edgeParents(edge))
        banListParents += parent
    }
    val srcs = edgesInArb.map(_._1)
    if (srcs.size == k) {
      timesATour += 1
      // Untested code, also takes O(n^2), can be O(n)
      /*
      var trip: List[NodeID] = List(rId)
      while (trip.size < k + 1) {
        trip ::= edgesInArb.find(_._1 == trip.head).get._2
      }
      trip = trip.reverse
      */
    }
    totalCost + edgesFromExtraNode(root)
  }
}
