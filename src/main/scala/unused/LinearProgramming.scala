package unused

import scala.collection.Seq

// Linear programming TSP solver which calls lp_solve from the command line
// Performance: very poor
class LinearProgramming(adj: Seq[Seq[Int]]) {
  val n: Int = adj.size

  // separator between variables, e.g. v12${s}1 = v12_1
  val s = "_"

  // objective function: minimize the sum of the chosen edges
  lazy val obj: String = {
    val terms = for (i <- 1 to n; j <- 1 to n; if i != j)
      yield s"${adj(i)(j)} v$i$s$j"
    s"min: ${terms.mkString(" + ")};\n\n"
  }

  def fromToConstraints(format: (Int, Int) => String): String = {
    (for (i <- 1 to n) yield {
      val terms = for (j <- 1 to n if j != i) yield format(i, j)
      terms.mkString(" + ") + " = 1;\n"
    }).mkString("") + "\n"
  }

  // ensure that there is only one outgoing edge for each node
  lazy val fromConstraints: String = fromToConstraints((i, j) => s"v$i$s$j")
  // ensure that there is only one incoming edge for each node
  lazy val toConstraints: String = fromToConstraints((i, j) => s"v$j$s$i")

  lazy val tourConstraints: String = {
    (for (i <- 2 to n; j <- 2 to n; if i != j) yield {
      s"u$i - u$j + ${n - 1} v$i$s$j + ${n - 3} v$j$s$i <= ${n - 2};\n"
    }).mkString("") + "\n"
  }

  lazy val uValueConstraints: String = {
    (for (i <- 2 to n) yield {
      s"u$i >= 1;\n" +
        s"u$i <= $n;\n"
    }).mkString("") + "\n"
  }
  // n^2 v values, each is a boolean and represents whether an edge is in our tour
  lazy val vValueConstraints: String = {
    (for (i <- 1 to n; j <- 1 to n; if i != j)
      yield s"bin v$i$s$j;\n"
      ).mkString("")
  }

  lazy val lpSolveModel: String = {
    obj + fromConstraints + toConstraints + tourConstraints +
      uValueConstraints + vValueConstraints
  }

  // Input: an lp_solve model, such as one generated
  //        from some methods in this object
  // Sends the model to lp_solve
  // Output: the output objective function
  //         (note: the actual solution is not included)
  def lpSubroutine(model: String): Float = {
    import sys.process._
    val res = (Seq("echo", model) #| Seq("lp_solve", "-S1")).!!
    res.split("\n")(1).split(" ").last.toFloat / 1000000
  }
}
