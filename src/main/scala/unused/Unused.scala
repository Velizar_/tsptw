package unused

import scala.annotation.tailrec
import scala.collection.immutable.NumericRange
import scala.collection.mutable.Buffer
import scala.collection.parallel.CollectionConverters._
import scala.collection.Seq

import tsp.TSP.dprintln
import tsp.TSP
import tsp.AdjMatrix
import algorithms.HungarianAlgorithm

/**
  * Created by velizar on 28/11/16.
  *
  * Contains code which got obsolete by a different implementation
  * which achieves the same purpose more efficiently;
  *
  * Or unfinished, shelved functionality.
  */
object Unused {
  import scala.collection.mutable.{ BitSet => MBitSet, ArrayBuffer, Map, PriorityQueue }
  import scala.collection.immutable.{ BitSet => IMBitSet }
  import tsp.BitSetObject._
  // O(n * k) with the pre-sorted adjacency matrix
  // n is the total number of nodes in it,
  // k is the number of relevant elements
  def solveMSTKruskal(sortedIndexedAdj: Seq[Seq[(Float, Byte)]], nodeIDbitset: BitSet): Float = {
    val nodeIDs = bitSetToQueue(nodeIDbitset).toVector
    val unionFind: UnionFind_[Byte] = new UnionFind_
    val minEdges = new PriorityQueue[(Float, (Byte, Byte))]()(Ordering.by(-_._1))
    val passedIdx: Map[Byte, Int] = Map(nodeIDs zip nodeIDs.map(x => 0): _*)
    var totalDist = 0.toFloat

    def addFromNode(from: Byte): Unit = {
      if (passedIdx(from) != sortedIndexedAdj(from).size) {
        val (dist, to) = sortedIndexedAdj(from)(passedIdx(from))
        passedIdx(from) += 1
        minEdges += ((dist, (from, to)))
      }
    }

    @tailrec
    def nextEdge(): (Float, (Byte, Byte)) = {
      val (dist, (from, to)) = minEdges.dequeue()
      addFromNode(from)
      if ((unionFind contains to) && (!unionFind.same(from, to)))
        (dist, (from, to))
      else
        nextEdge()
    }

    // populate edges
    nodeIDs.foreach(unionFind.add)
    nodeIDs.foreach(addFromNode)

    var i = 0
    while (i < nodeIDs.size - 1) {
      val (dist, (from, to)) = nextEdge()
      if (!unionFind.same(from, to)) {
        unionFind.union(from, to)
        totalDist += dist
        i += 1
      }
    }

    totalDist
  }

  // O(n * k) with the pre-sorted adjacency matrix
  // n is the total number of nodes in it,
  // k is the number of relevant elements
  def solveMSTPrim(sortedIndexedAdj: Seq[Seq[(Float, Byte)]], nodeIDbitset: BitSet/*,
                   skipped: Byte = 0.toByte, skippedFrom: Byte = 0.toByte*/): Float = {
    var remainingNodeIDs = nodeIDbitset
    val minEdges = new PriorityQueue[(Float, (Byte, Byte))]()(Ordering.by(-_._1))
    val passedIdx: Map[Byte, Int] = Map.empty.withDefaultValue(0)
    var totalDist = 0.toFloat

    def addFromNode(from: Byte): Unit = {
      val (dist, to) = sortedIndexedAdj(from)(passedIdx(from))
      passedIdx(from) += 1
      minEdges += ((dist, (from, to)))
    }

    def nextEdge(): (Float, (Byte, Byte)) = {
      val (dist, (from, to)) = minEdges.dequeue()
      if (bitSetContains(remainingNodeIDs, to)) { // ensures the edge is right
        (dist, (from, to))
      } else {
        addFromNode(from)
        nextEdge()
      }
    }

    var lastFrom: Byte = -1

    if (remainingNodeIDs != 0) {
      // initialize with an arbitrary byte
      val b = getFirstByte(remainingNodeIDs)
      remainingNodeIDs = bitSetRemove(remainingNodeIDs, b)
      lastFrom = b // adding it first makes it the only src in the heap
    }

    while (remainingNodeIDs != 0) {
      addFromNode(lastFrom)
      val (dist, (from, to)) = nextEdge()
      lastFrom = from
      remainingNodeIDs = bitSetRemove(remainingNodeIDs, to)
      addFromNode(to)
      totalDist += dist
    }

    totalDist
  }

  // Assuming that the MST is a path, find the MST length and its two endpoints
  def mstPrimPath(sortedIndexedAdj: Seq[Seq[(Float, Byte)]], nodeIDbitset: BitSet): (Float, Set[Byte]) = {
    var remainingNodeIDs = nodeIDbitset
    val passedIdx: Map[Byte, Int] = Map.empty.withDefaultValue(0)
    var totalDist = 0.toFloat

    var first: Byte = -1
    var second: Byte = -1

    var firstBest: (Float, Byte) = (0, -1)
    var secondBest: (Float, Byte) = (0, -1)

    def next(x: Byte): (Float, Byte) = {
      passedIdx(x) += sortedIndexedAdj(x).drop(passedIdx(x)).takeWhile({
        case (_, dst) => !bitSetContains(remainingNodeIDs, dst)
      }).size
      val out = sortedIndexedAdj(x)(passedIdx(x))
      passedIdx(x) += 1
      out
    }

    if (remainingNodeIDs != 0) {
      val b = getFirstByte(remainingNodeIDs)
      remainingNodeIDs = bitSetRemove(remainingNodeIDs, b)
      first = b
    }

    if (remainingNodeIDs != 0) {
      val (dist, to) = next(first)
      remainingNodeIDs = bitSetRemove(remainingNodeIDs, to)
      totalDist += dist
      if (remainingNodeIDs != 0) {
        second = to
        firstBest = next(first)
        secondBest = next(second)
      }
    }

    while (remainingNodeIDs != 0) {
      if (firstBest._1 < secondBest._1) {
        if (!bitSetContains(remainingNodeIDs, firstBest._2)) {
          firstBest = next(first)
        } else {
          totalDist += firstBest._1
          first = firstBest._2
          remainingNodeIDs = bitSetRemove(remainingNodeIDs, first)
          if (remainingNodeIDs != 0)
            firstBest = next(first)
        }
      } else {
        if (!bitSetContains(remainingNodeIDs, secondBest._2)) {
          secondBest = next(second)
        } else {
          totalDist += secondBest._1
          second = secondBest._2
          remainingNodeIDs = bitSetRemove(remainingNodeIDs, second)
          if (remainingNodeIDs != 0)
            secondBest = next(second)
        }
      }
    }

    (totalDist, Set(first, second))
  }

  def solveTspBruteforce(adj: AdjMatrix): (Int, Seq[Byte]) = {
    val tours = for (tour <- NumericRange(1: Byte, adj.n, 1: Byte).permutations)
      yield (TSP.length(0.toByte +: tour, adj), tour)
    val (cost, path) = tours.minBy(_._1)
    (cost, 0.toByte +: path.toArray)
  }

  // Applies the patching algorithm proposed by Karp (1979)
  // Heuristic for O(n^3), starts from assignment problem then it improves it
  // Ignores time windows
  def solveTspPatching(adj: AdjMatrix = null): (Int, Seq[Byte]) = {
    val apAdj = adj.twoD.toArray.map(_.toArray.map(_.toDouble))
    for (i <- 0 until adj.n)
      apAdj(i)(i) = Int.MaxValue
    val apTour: Array[Int] = new HungarianAlgorithm(apAdj).execute()

    // path is the same format as HungarianAlgorithm's output
    // assumes that path is one or more cycles
    def getSubtours(tourSeq: Seq[Int]): Seq[Seq[(Int, Int)]] = {
      val tour = tourSeq.toArray.clone()
      val subtours = Buffer.empty[Buffer[(Int, Int)]]
      for (i <- tour.indices) {
        var current = i
        var next = tour(current)
        if (next != -1) {
          val subtour = Buffer.empty[(Int, Int)]
          while (next != -1) {
            tour(current) = -1
            subtour += ((current, next))
            current = next
            next = tour(current)
          }
          subtours += subtour
        }
      }
      subtours
    }
    val cycles = getSubtours(apTour).sortBy(-_.length)

    def mergeCycles(a: Seq[(Int, Int)], b: Seq[(Int, Int)]): Seq[(Int, Int)] = {
      val choices = for {
        idx1 <- a.indices
        idx2 <- b.indices
        (i, j) = a(idx1)
        (k, l) = b(idx2)
        cost = adj(i, l) + adj(k, j) - adj(i, j) - adj(k, l)
      } yield (cost, idx1, idx2)
      val (_, idx1, idx2) = choices.minBy(_._1)
      val (i, j) = a(idx1)
      val (k, l) = b(idx2)

      a.take(idx1) ++ Seq((i, l)) ++ b.drop(idx2 + 1) ++ b.take(idx2) ++ Seq((k, j)) ++ a.drop(idx1 + 1)
    }

    val mergedTour = cycles.reduceLeft(mergeCycles)
    val (end, start) = mergedTour.span{ case(x, _) => x != 0 }
    val normalizedTour = (start ++ end).map(_._1.toByte)

    (TSP.length(normalizedTour, adj), normalizedTour)
  }

  def debugSuperNode(sn: SuperNode): String = sn match {
    case Cycle(nodes) => "Cycle:\n" +
      nodes.flatMap{node => debugSuperNode(node._1).map("  " + _)}.mkString("\n")
    case Node(id) => id.toString
  }

  type NodeID = Byte
  type CycleEdge = List[Byte]
  trait SuperNode
  case class Node(node: NodeID) extends SuperNode
  // The first Float is the incoming edge cost
  // Each node has one outgoing edge; that edge also points to a node in all of the deeper levels
  type CycleNodes = List[(SuperNode, Float, CycleEdge)]
  case class Cycle(nodes: CycleNodes) extends SuperNode
}

/** A crude implementation of the union-find data structure
  *
  * O(1) for find(e) and O(n) for union(e1, e2)
  * where n is the size of the smaller of (e1, e2)
  * Duplicate keys are not allowed
  */
class UnionFind_[T] {
  import collection.mutable.{Map, ArrayBuffer}

  // maps from element (key) to its internal index
  private val eToK: Map[T, Int] = Map.empty
  private val followers: Map[Int, ArrayBuffer[Int]] = Map.empty
  private val leader: ArrayBuffer[Int] = ArrayBuffer.empty

  def size = leader.size
  def add(e: T): Boolean = {
    if (eToK contains e) {
      false // no duplicates allowed
    } else {
      val key = this.size
      eToK += ((e, key))
      followers(eToK(e)) = ArrayBuffer(key)
      leader += key
      true
    }
  }

  def union(e1: T, e2: T): Unit = {
    unionByKey(eToK(e1), eToK(e2))
  }

  def unionByKey(key1: Int, key2: Int): Unit = {
    val l1 = leader(key1)
    val l2 = leader(key2)
    unionByLeader(l1, l2)
  }

  def unionByLeader(l1: Int, l2: Int): Int = {
    if (l1 != l2) {
      val (smaller, bigger) = if (followers(l1).size < followers(l2).size) (l1, l2) else (l2, l1)
      followers(bigger) ++= followers(smaller)
      for (k <- followers(smaller))
        leader(k) = bigger
      followers(smaller) = ArrayBuffer.empty
      bigger
    } else {
      l1
    }
  }

  def unionMultiple(e: Seq[T]): Unit = {
    unionMultipleByKey(e.map(eToK))
  }

  def unionMultipleByKey(e: Seq[Int]): Unit = {
    val leaders = e.map(leader).view.toSet.toSeq
    unionMultipleByLeader(leaders)
  }

  // returns the new leader
  def unionMultipleByLeader(leaders: Seq[Int]): Int = {
    if (leaders.size != 1) {
      val newLeader = leaders.maxBy(followers(_).size)
      for (chngLeader <- leaders if chngLeader != newLeader) {
        followers(newLeader) ++= followers(chngLeader)
        for (k <- followers(chngLeader))
          leader(k) = newLeader
        followers(chngLeader) = ArrayBuffer.empty
      }
      newLeader
    } else {
      leaders.head
    }
  }

  /** returns -1 if not found */
  def find(e: T): Int = eToK get e match {
    case Some(k) => leader(k)
    case None    => -1
  }

  def removeCluster(rmLeader: Int) = {
    followers(rmLeader) = ArrayBuffer.empty
    for (i <- leader.indices if leader(i) == rmLeader)
      leader(i) = -1
  }

  // returns all members of the cluster
  def members(leader: Int): Seq[T] = followers(leader).map(kToE(_))

  def numClusters = followers.values.filter(_ != ArrayBuffer.empty).toSeq.size

  def toMap: Map[Int, ArrayBuffer[T]] = {
    val k = kToE
    followers.map({case (leader_, buf) =>
      (leader_, buf.map(k))
    })
    /*  val imMap = eToK.map { case(e, k) =>
          (e, leader(k))
        }.groupBy(_._2).mapValues(_.keys.toSeq)
        Map(imMap.toSeq: _*)*/
  }

  private def kToE: Map[Int, T] =
    eToK.map { case (e, k) => (k, e) }

  // Methods for Kruskal

  def same(e1: T, e2: T): Boolean =
    (find(e1) != -1) && (find(e1) == find(e2))

  def contains(e1: T): Boolean = find(e1) != -1
}

object BranchAndBound_ {

  import scalaz.Memo.immutableHashMapMemo
  import scala.collection.immutable.NumericRange
  import tsp.BitSetObject._
  import tsp.TSP.NodeID // = Byte

  var n: Byte = _
  var adj: AdjMatrix = _

  val sortedOutgoingEdges: Vector[Vector[(Float, Byte)]] = adj.twoD.map{x => x.map(_.toFloat) zip NumericRange(0: Byte, n, 1: Byte) sortBy (_._1) drop 1}
  val sortedIncomingEdges: Vector[Vector[(Float, Byte)]] = adj.twoD.transpose.map{x => x.map(_.toFloat) zip NumericRange(0: Byte, n, 1: Byte) sortBy (_._1) drop 1}
  val allNodes: BitSet = math.pow(2, n - 1).toInt - 1
  val relaxation = 1.7.toFloat

  // performs horribly: computes slowly and gives bad estimates
  val apCache = immutableHashMapMemo[(BitSet, NodeID), Int] { case (remaining, last) =>
    val array = adj.twoD.zipWithIndex.map { case (src, i) =>
      // i is passed
      if (!bitSetContains(remaining, i.toByte) && i != last) {
        val output = Array.fill(n)(Double.MaxValue)
        output(i) = 0d
        output
      } else if (i == 0) {
        val output = Array.fill(n)(Double.MaxValue)
        output(last) = 0d
        output
      } else {
        // i is in remaining ++ { last }
        src.zipWithIndex.map { case (dist, j) =>
          if (i == j) {
            Double.MaxValue
          } else if ((i, j) == (last, 0)) {
            Double.MaxValue
          } else if (j == last) {
            Double.MaxValue
          } else if (j == 0) {
            dist.toDouble
          } else if (!bitSetContains(remaining, j.toByte)) {
            Double.MaxValue
          } else {
            dist.toDouble
          }
        }.toArray
      }
    }.toArray
    val res = new HungarianAlgorithm(array).execute()
    (last +: bitSetToQueue(remaining)).map{ i =>
      adj(i, res(i).toByte)
    }.sum
  }

  val mstCache = immutableHashMapMemo[BitSet, Float] {
    Unused.solveMSTPrim(sortedOutgoingEdges, _)
  }

  /** Out-going edges heuristic
    *
    * In order to solve a TSP with the set of nodes which aren't included,
    * each node will need to have exactly two edges, however 1/2 of their sum
    * is counted because each edge includes another node. Note that we are
    * taking the two smallest edges (rather than the smallest one twice).
    *
    * There is a problem with the current solution: given k remaining
    * nodes, it connects them with k edges. However, in our case they will
    * have k-1 edges between them and two more to connect to our last
    * node and to our 0th node. We are still doing k+1 edges in total,
    * because we are counting the two edges which connect our solution
    * to the remaining edges and halving them. We should instead discard
    * the 2 weightiest edges between the remaining nodes and instead
    * fully count the edges from last and 0, but that gives a much worse
    * performance. Which is why this heuristic is potentially breaking,
    * but useful for approximate BnB solutions.
    *
    * Notice `pq / relaxation` - if relaxation = 2, this would work properly;
    * however it is less than 2 which makes the heuristic pessimistic, but the
    * overall algorithm extremely fast and suboptimal - slower but more accurate
    * than the insertion heuristic.
    */
  val outgoingEdgesHeuristicMemo = immutableHashMapMemo[(BitSet, NodeID), Float] {
    case (prevSubset, last) =>
      val remainingNodes = allNodes - prevSubset - intToBitPos(last)
      // double of the optimistic estimate
      var heuristic: Float = 0
      // add the two cheapest outgoing edges per node
      for (node <- bitSetToQueue(remainingNodes).view) {
        var idx = 0
        var encountered = 0
        val subAdj = sortedOutgoingEdges(node).view
        while (encountered != 2) {
          // get the cheapest outgoing node
          val (cost, id) = subAdj(idx)
          // skip node if it's in prevSubset
          if (!bitSetContains(prevSubset, id)) {
            encountered += 1
            heuristic += cost
          }
          idx += 1
        }
      }

      // Also count the edges to 0 and from last
      if (remainingNodes != 0) {
        val biggerPrevSubset = prevSubset + intToBitPos(last)
        val shortestFromZero = sortedIncomingEdges(0).dropWhile(x => bitSetContains(biggerPrevSubset, x._2)).head
        val shortestFromLast = sortedOutgoingEdges(last).dropWhile(x => bitSetContains(prevSubset, x._2) || x._2 == 0).head
        heuristic += shortestFromZero._1
        heuristic += shortestFromLast._1
      }

      heuristic / relaxation
  }
}

/** MST calculation with caching of partially completed MSTs
  *
  * Not thread safe yet.
  * Needs a few optimizations in order to beat the other algorithm.
  *
  * How it works:
  *   Starting with the smallest ID node you have in nodeIDbitset,
  *   the shortest edge is found and then if the corresponding node
  *   is in nodeIDbitset, it takes a turn right and includes it, otherwise
  *   a left turn; the process is repeated until each node is mentioned or
  *   until everything in nodeIDbitset is covered. This allows for reuse of
  *   calculations because the subtrees are completely non-overlapping and
  *   the MST is computed by transitioning into reusable states.
  *
  * In practice it currently seems about 2x as fast as the non-cached
  *   version, both running on 1 process. It is also expected to take
  *   more RAM in order to store the cached steps.
  */
object MemoTree {
  import scala.annotation.tailrec
  import collection.mutable.PriorityQueue
  import tsp.BitSetObject._

  // TODO: consider renaming 'Memo'
  type IMMap[A, B] = collection.immutable.Map[A, B]
  type MemoStack = (PriorityQueue[(Float, (Byte, Byte))], IMMap[Byte, Int], Float)

  case class MemoBranch(branch: Byte, dist: Float,
                        var left: Either[MemoStack, MemoBranch],
                        var right: Either[MemoStack, MemoBranch])

  def calcTree(passedNodeIDs: BitSet, minEdges: PriorityQueue[(Float, (Byte, Byte))],
               passedIndexes: IMMap[Byte, Int], totalDist: Float,
               sortedIndexedAdj: Seq[Seq[(Float, Byte)]]): MemoBranch = {
    var passedIdx = passedIndexes
    def addFromNode(from: Byte): IMMap[Byte, Int] = {
      if (passedIdx(from) < n) {
        val (dist, to) = sortedIndexedAdj(from)(passedIdx(from))
        minEdges += ((dist, (from, to)))
        passedIdx.updated(from, passedIdx(from) + 1)
      } else {
        passedIdx
      }
    }

    val (dist, (from, to)) = minEdges.dequeue()
    passedIdx = addFromNode(from)

    if (bitSetContains(passedNodeIDs, to) || to == 0) {
      // try again
      calcTree(passedNodeIDs, minEdges, passedIdx, totalDist, sortedIndexedAdj)
    } else {
      // branch off based on whether we want `to` or not
      val (minEdgesCopy, passedIdxCopy) = (minEdges.clone(), passedIdx) // for the left branch
      passedIdx = addFromNode(to)                                       // for the right branch

      // TODO: make the branches lazy
      MemoBranch(to, totalDist,
        Left(minEdgesCopy, passedIdxCopy, totalDist),
        Left(minEdges, passedIdx, totalDist + dist))
    }
  }

  @tailrec
  def getMSTMemo(remainingNodeIDs: BitSet, passed: BitSet, tree: MemoBranch,
                 sortedIndexedAdj: Seq[Seq[(Float, Byte)]]): Float = {
    if (remainingNodeIDs == 0) {
      tree.dist
    } else {
      val b = tree.branch
      val newPassed = passed + intToBitPos(b)
      if (bitSetContains(remainingNodeIDs, b)) {
        val newRemainingNodeIDs = bitSetRemove(remainingNodeIDs, b)
        tree match {
          case MemoBranch(_, _, _, Right(r)) => getMSTMemo(newRemainingNodeIDs, newPassed, r, sortedIndexedAdj)
          case tree@MemoBranch(_, _, _, Left((pq, idx, d))) => {
            if (newPassed == math.pow(2, n) - 1) {
              d
            } else {
              val newTree = calcTree(newPassed, pq.clone(), idx, d, sortedIndexedAdj)
              tree.right = Right(newTree)
              getMSTMemo(newRemainingNodeIDs, newPassed, newTree, sortedIndexedAdj)
            }
          }
        }
      } else {
        tree match {
          case MemoBranch(_, _, Right(l), _) => getMSTMemo(remainingNodeIDs, newPassed, l, sortedIndexedAdj)
          case tree@MemoBranch(_, _, Left((pq, idx, d)), _) => {
            val newTree = calcTree(newPassed, pq.clone(), idx, d, sortedIndexedAdj)
            tree.left = Right(newTree)
            getMSTMemo(remainingNodeIDs, newPassed, newTree, sortedIndexedAdj)
          }
        }
      }
    }
  }

  var memoTrees: Array[MemoBranch] = Array.empty
  var n: Byte = -1

  def mstPrimInit(sortedIndexedAdj: Seq[Seq[(Float, Byte)]]): Unit = {
    this.n = sortedIndexedAdj(0).size.toByte

    this.memoTrees = ((for {
      i <- 1 until n
      pq = new PriorityQueue[(Float, (Byte, Byte))]()(Ordering.by(-_._1))
      (dist, to) = sortedIndexedAdj(i)(0)
      _ = pq.enqueue((dist, (i.toByte, to)))
    } yield calcTree(
      passedNodeIDs = seqToBitSet((1 to i).map(_.toByte)),
      minEdges      = pq,
      passedIndexes = collection.immutable.Map((i.toByte, 1)).withDefaultValue(0),
      totalDist     = 0,
      sortedIndexedAdj)
      ) ++ Seq(MemoBranch(0, 0, null, null))).toArray
  }

  def solveMSTPrimReuse(sortedIndexedAdj: Seq[Seq[(Float, Byte)]], nodeIDbitset: BitSet): Float = {

    // TODO: Try with an immutable heap (sorted set or from FingerTree)

    if (nodeIDbitset == 0) {
      0
    } else {
      val first = getFirstByte(nodeIDbitset)
      getMSTMemo(
        nodeIDbitset - intToBitPos(first),
        intToBitPos(first),
        memoTrees(first - 1),
        sortedIndexedAdj)
    }
  }
}

/**
  * Created by Velizar on 01/09/16.
  *
  * aTSP (asymmetric travelling salesman problem) algorithm
  * Based on Carpaneto and Toth (1980)
  *
  * Carpaneto, G., and Toth, P. (1980), "Some new branching and
  * bounding criteria for the asymmetric travelling salesman problem",
  * Management Science 26, 736-743.
  */
object CarpanetoToth {

  import java.util.concurrent.PriorityBlockingQueue
  import scala.collection.mutable.Buffer

  val parallelism = 1

  def solve(adj: Array[Array[Double]]): Option[(Double, Seq[Int])] = {
    var apCalls = 0

    type NodeID = Int
    type Edge   = (NodeID, NodeID)
    type Tour   = Array[NodeID]
    case class State(apTour: Tour, heuristic: Double, included: Set[Edge], excluded: Set[Edge])

    val maxValue = adj.map(_.max).sum
    var bestLength = Double.MaxValue
    var minTour: Option[Tour] = None

    // Assumes that tour is one or more cycles
    def smallestSubtour(_tour: Tour, incl: Set[Edge] = Set.empty): Seq[Edge] = {
      val tour = _tour.clone()
      val subtours = Buffer.empty[Buffer[Edge]]
      for (i <- tour.indices) {
        var current = i
        var next = tour(current)
        if (next != -1) {
          val subtour = Buffer.empty[Edge]
          while (next != -1) {
            tour(current) = -1
            if (!incl.contains((current, next)))
              subtour += ((current, next))
            current = next
            next = tour(current)
          }
          subtours += subtour
        }
      }
      subtours.minBy(_.size)
    }

    def containsSubtours(tour: Tour): Boolean =
      smallestSubtour(tour).length != tour.length

    def rowColHeuristic(adj: Array[Array[Double]]): Double = {
      val minRows = adj.map(_.min)
      val rowReduced = adj.zip(minRows).map{ case (row, min) => row.map(_ - min) }
      val minCols = rowReduced.transpose.map(_.min)
      minRows.sum + minCols.sum
    }

    /**
      * Augments adj so that any assignment violating incl or excl
      * will include a node with maxValue
      */
    def applyIncludedAndExcluded(adj: Array[Array[Double]], incl: Iterator[Edge], excl: Iterator[Edge]): Array[Array[Double]] = {
      val newAdj = adj.map(_.clone)
      incl.foreach { case (i, j) =>
        newAdj(i).indices.foreach { h =>
          newAdj(i)(h) = maxValue
          newAdj(h)(j) = maxValue
        }
        newAdj(i)(j) = 0
      }

      excl.foreach { case (i, j) =>
        newAdj(i)(j) = maxValue
      }

      newAdj
    }

    def edgesLength(edges: Seq[Edge]) = edges.map { case (source, destination) =>
      adj(source)(destination)
    }.sum

    def length(tour: Tour) = edgesLength(tour.indices zip tour)

    def ap(adj: Array[Array[Double]]): Array[Int] = {
      apCalls += 1
      new HungarianAlgorithm(adj).execute()
    }

    def nextStates(state: State): Seq[State] = {
      val subtour = smallestSubtour(state.apTour, state.included).toSet
      val el = subtour.take(1)
      for {
        newIncluded <- Seq(Set.empty[Edge], Set(subtour.head))//el.subsets.toSeq.view
        //        newIncluded <- subtour.subsets.toSeq.view
        if newIncluded.size != subtour.size
        newExcluded = el diff newIncluded
        //        newExcluded = subtour diff newIncluded
        included = newIncluded ++ state.included
        excluded = newExcluded ++ state.excluded
        (apTour, heuristic) = {
          if (newExcluded.isEmpty)
            (state.apTour, state.heuristic)
          else {
            val augAdj = applyIncludedAndExcluded(adj, included.iterator, excluded.iterator)
            val tour = ap(augAdj)
            (tour, length(tour))
          }
        }
        //        if (rowColHeuristic(augAdj) + edgesLength(included.toSeq)) < bestLength
        if heuristic < bestLength
        isValid = !containsSubtours(apTour)
        _ = if (isValid) {
          // TODO: refactor bestLength and minTour to a value which is passed around, rather than a global
          bestLength = heuristic
          minTour = Some(apTour)
        }
        if !isValid
      } yield State(apTour, heuristic, included, excluded)
    }

    /* solution */

    // never choose adj(i)(i) edges
    for (i <- adj.indices)
      adj(i)(i) = maxValue

    // n two-node states 0-x for x in 1 to n
    val fringe = new PriorityBlockingQueue[State](1500, Ordering.by(_.heuristic))
    val firstTour = ap(adj)
    val firstState = State(firstTour, length(firstTour), Set.empty, Set.empty)
    if (!containsSubtours(firstTour)) {
      minTour = Some(firstTour)
    } else {
      fringe.add(firstState)
    }

    var terminate = fringe.isEmpty
    while (!terminate) {
      val state = fringe.take()
      if (state.heuristic < bestLength) {
        val next = nextStates(state)
        next.foreach(fringe.add)
        //        nextStates(state).foreach(fringe.add)
      } else {
        terminate = true
      }
    }
    dprintln("Fringe size: " + fringe.size)
    dprintln("AP calls: " + apCalls)

    minTour.map { solution =>
      // use visited_1, visited_2, ... visited_n
      // instead of destination_from_1, destination_from_2, ... destination_from_n
      // e.g. 0, 3, 4, 2, 1 instead of 3, 0, 1, 4, 2
      val normalizedSolution = 0 +: smallestSubtour(solution).map(_._2).init
      (length(solution), normalizedSolution)
    }
  }
}
