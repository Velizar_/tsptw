package unused

type NodeID = Byte
type EdgeWithDst = (Int, NodeID, NodeID)
// Can be made generic instead of specific to EdgeAlt, but then addToAll will need extra work
object IMSkewHeap {
  def empty(using ord: Ordering[EdgeWithDst]): IMSkewHeap = EmptyIMSkewHeap()
  def apply(elem: EdgeWithDst): IMSkewHeapNode = IMSkewHeapNode(elem, empty, empty)
}
sealed trait IMSkewHeap {
  def min: EdgeWithDst
  def tail: IMSkewHeap
  def insert(elem: EdgeWithDst): IMSkewHeapNode
  def merge(that: IMSkewHeap): IMSkewHeap
  def addToAll(value: Int): IMSkewHeap
}
case class IMSkewHeapNode(head: EdgeWithDst, left: IMSkewHeap, right: IMSkewHeap, addedValue: Int = 0)(using ord: Ordering[EdgeWithDst]) extends IMSkewHeap {
  override lazy val min: EdgeWithDst = (head._1 + addedValue, head._2, head._3)
  override lazy val tail: IMSkewHeap = left.addToAll(addedValue).merge(right.addToAll(addedValue))
  override def insert(elem: EdgeWithDst): IMSkewHeapNode = IMSkewHeap(elem).merge(this)
  override def merge(that: IMSkewHeap): IMSkewHeapNode = that match {
    case EmptyIMSkewHeap() => this
    case that: IMSkewHeapNode =>
      val (h1, h2) = if ord.lt(min, that.min) then (this, that) else (that, this)
      IMSkewHeapNode(h1.min, h2.merge(h1.right.addToAll(h1.addedValue)), h1.left.addToAll(h1.addedValue))
  }
  override def addToAll(value: Int): IMSkewHeap = if (value == 0) this else copy(addedValue = addedValue + value)
}
case class EmptyIMSkewHeap()(using ord: Ordering[EdgeWithDst]) extends IMSkewHeap {
  override def min: EdgeWithDst = throw new NoSuchElementException("empty.min")
  override def tail: IMSkewHeap = throw new NoSuchElementException("empty.tail")
  override def insert(elem: EdgeWithDst): IMSkewHeapNode = IMSkewHeap(elem)
  override def merge(that: IMSkewHeap): IMSkewHeap = that
  override def addToAll(value: Int): IMSkewHeap = this
}
