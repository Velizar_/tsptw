package unused

import tsp.AdjMatrix

import scala.collection.immutable.NumericRange
import tsp.BitSetObject.*
import tsp.TSP.NodeID
import tsp.ByteArbrscUnionFind

import scala.collection.mutable.PriorityQueue
import scala.math.Ordered.orderingToOrdered

type Edge = (Int, NodeID)

/** Alternative version of min spanning arborescence that uses heaps
  *
  * This is much closer to the original paper's implementation.
  * However it is slower than what I had before, and also more opaque.
  *
  * It might be faster with a different type of heap.
  */
object MinArborescenceUsingHeaps {
  private var skewHeaps: Array[IMSkewHeap] = _

  def init(adj: AdjMatrix): Unit = {
    given ord: Ordering[(Int, Byte)] = Ordering.by(_._1)
    skewHeaps = Array.fill(adj.n * 2 - 1)(IMSkewHeap.empty)
    for (dst <- 1 until adj.n; src <- 1 until adj.n if src != dst)
      skewHeaps(dst) = skewHeaps(dst).insert((adj(src, dst), src.toByte, dst.toByte))
  }

  type SuperNodeID = NodeID

  // Note: subgraph never includes 0
  def minArborescence(subgraph: BitSet, edgesFromR: Seq[Int]): Int = {
    if (subgraph == 0)
      return 0

    val subgraphVec = bitSetToQueue(subgraph).toVector
    val k = subgraphVec.size
    val unionFind = new ByteArbrscUnionFind(subgraphVec.lastOption.getOrElse(-1), k.toByte)
    // a heap of min incoming edges for each supernode
    val inEdges: Array[IMSkewHeap] = skewHeaps.clone
    // keeps track of the cost to insert an edge from the extra root for each supernode
    val edgesFromExtraNode: Array[Int] = edgesFromR.toArray ++ new Array((unionFind.last + k - edgesFromR.size) max 0)
    var totalCost = 0

    // contains the current arborescence in progress
    var growthPath: List[SuperNodeID] = List(subgraphVec.head)
    def root = growthPath.head
    var nodesOnPath: BitSet = intToBitPos(root)

    // each iteration, add the min-cost node connecting to the supernode under growthPath(lastIdx)
    while (!(growthPath.tail == Nil && nodesOnPath == subgraph)) {
      // skip invalid edges
      while (!bitSetContains(subgraph, inEdges(root).min._2) || unionFind.find(inEdges(root).min._2) == unionFind.find(root))
        inEdges(root) = inEdges(root).tail
      val (addedCost, next, _) = inEdges(root).min
      inEdges(root) = inEdges(root).tail
      inEdges(root) = inEdges(root).addToAll(-addedCost)
      edgesFromExtraNode(root) -= addedCost
      totalCost += addedCost
      if (bitSetContains(nodesOnPath, next)) {
        // we just formed a cycle, merge it into a supernode
        val (nodesBeforeNext, parentOfNext :: newGrowthPath) = growthPath.span(unionFind.find(_) != unionFind.find(next))
        growthPath = newGrowthPath
        val nodesToMerge = parentOfNext :: nodesBeforeNext
        val newSuperNode = unionFind.union(nodesToMerge)
        edgesFromExtraNode(newSuperNode) = Int.MaxValue
        inEdges(newSuperNode) = IMSkewHeap.empty
        for (node <- nodesToMerge) {
          inEdges(newSuperNode) = inEdges(newSuperNode) merge inEdges(node)
          edgesFromExtraNode(newSuperNode) = edgesFromExtraNode(newSuperNode) min edgesFromExtraNode(node)
        }
        growthPath ::= newSuperNode
      } else {
        nodesOnPath += intToBitPos(next)
        growthPath ::= next
      }
    }
    totalCost + edgesFromExtraNode(root)
  }
}
