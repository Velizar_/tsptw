package tsp

import scala.collection.immutable.NumericRange
import scala.collection.Seq
import scala.util.Random.shuffle

import scala.collection.parallel.CollectionConverters._
import scalaz.syntax.std.boolean._

import TSP.{ NodeID, dprintln, length, lengthWithLT, pathLengthWithLT, pathLength,
             feasibleTSPTW, solveTspInsertion }

/** Implements local search with several heuristics
  *
  * Runs k-swap, k-opt, and flip in this order.
  * Repeats each until it can no longer improve the tour in any possible way.
  * Then moves on to the next one.
  * If an improvement is found later, it starts all over.
  */
object LocalSearch {

  // performs local search { restarts + 1 } times, each on a fresh random tour
  val defaultRestarts = 9

  // the k in k-opt
  // 5-opt not worthwhile in practice: the run time is OK, but it seldom finds improvements over 4-opt
  val k = 4

  // minimum size to consider local search, otherwise fall back to exact solving
  val minSize = k + 1

  def localSearch(adj: AdjMatrix, initialTour: Seq[NodeID], k: Int): Seq[NodeID] = {
    // the output of tour(i: TourIdx) is j: ByteID
    type TourIdx = Byte

    var tour = initialTour.toArray.clone
    val n = tour.length
    // all inputs are TourIdx
    def c(i: Int, j: Int): Int = adj(tour(i), tour(j % n))
    def lt(i: Int): Int = adj.lt(tour(i))
    def mt(i: Int): Int = adj.mt(tour(i))

    dprintln(s"localSearch start length: ${lengthWithLT(tour, adj)}")

    /** Local optimization inner functions
      *
      * They all mutate the tour array, but always leave it in a valid state
      */
    // Reverses the path from i to j
    // Input:  Array(0, 1, 2, 3, 4, 5), 1, 4
    // Output: Array(0, 4, 3, 2, 1, 5)
    def flip(start: Int, end: Int): Unit = {
      def swap(i: Int, j: Int): Unit = {
        val jSwap = tour(j)
        tour(j) = tour(i)
        tour(i) = jSwap
      }

      var step = 0
      while (start + step < end - step) {
        swap(start + step, end - step)
        step += 1
      }
    }

    def flipCost(i: Int, j: Int): Option[Int] = {
      val swapOuterCost = c(i - 1, j) - c(i - 1, i) + c(i, j + 1) - c(j, j + 1)
      val startCost = pathLength(tour.take(i), adj)
      val innerOldCost = pathLengthWithLT(tour.slice(i, j + 1), adj, startCost + c(i - 1, i))._1
      val (innerNewCost, valid) = pathLengthWithLT(tour.slice(i, j + 1).reverse, adj, startCost + c(i - 1, j))

      valid option (swapOuterCost + innerNewCost - innerOldCost)
    }

    def kOpt(paths: Vector[Path], lastPath: PathT): (Vector[Path], Int) = {
      val first +: nextPaths = paths

      def mergePaths(cost: Int, prevEnd: TourIdx, wasValid: Boolean, path: Path): (Int, TourIdx, Boolean) = {
        val costToPath = cost + c(prevEnd, path.start)
        (Seq(costToPath, path.mt).max + path.cost, path.end, wasValid && costToPath <= path.lt)
      }

      val lastOutput = lastPath match {
        case p@Path(_, _, _, _, _) => Seq(p)
        case EmptyPath(_) => Seq.empty
      }

      (for {
        choice <- nextPaths.permutations
        (penultimateCost, lastNode, penultimateValid) = choice.foldLeft((first.cost + first.mt, first.end, true)) {
          case ((c, l, v), p) => mergePaths(c, l, v, p)
        }

        // include the last path and the edge to 0
        (cost, isValid) = lastPath match {
          case p@Path(_, _, _, _, _) =>
            val (pathCost, end, valid) = mergePaths(penultimateCost, lastNode, penultimateValid, p)
            (pathCost + c(end, 0), valid)
          case EmptyPath(_) =>
            (penultimateCost + c(lastNode, 0), penultimateValid)
        }
        if isValid
      } yield ((first +: choice) ++ lastOutput, cost)).minBy(_._2)
    }

    trait PathT {
      // end + 1 = the position of the hole
      def holePos: TourIdx
    }

    /** Denotes a connected part of the tour, becomes invalid when tour gets mutated
      *
      * @param start - the index of the first node
      * @param end   - the index of the last node; the next index is the hole
      * @param cost  - the sum edges, constraints are ignored
      * @param lt    - constraint for the whole path, if you pass it then you pass all LT
      * @param mt    - analogous to LT
      *
      * The LT and MT are calculated by assuming that the input tour was valid and
      * taking the most constraining value from each node. They do not interact. The
      * only corner case is when there is an MT before an LT, where the MT is higher
      * than the LT - this is possible in valid tours. The check for whether the LT
      * constraint passes must be applied before the length is modified to reflect
      * the MT constraint, because in the opposite order, the code will be incorrect in
      * this corner case.
      */
    case class Path(start: TourIdx, end: TourIdx, cost: Int, lt: Int, mt: Int) extends PathT {
      def holePos: TourIdx = (end + 1).toByte
    }
    case class EmptyPath(holePos: TourIdx) extends PathT

    /** Returns all the possible ways to remove k nodes and properties of each combination
      *
      * @param k - the k in k-opt; the number of nodes being removed
      * The other parameters are for recursive calls only.
      * Assumes that tour is valid.
      */
    def kHoles(k: Int, start: TourIdx = 0.toByte, prevPaths: Vector[PathT] = Vector.empty): LazyList[Vector[PathT]] = {
      val range: LazyList[TourIdx] = NumericRange((start + 1).toByte, (adj.n - k).toByte, 1: Byte).to(LazyList)
      if (k == 0) {
        // here, the hole is out of bounds (because there is no hole, just the remaining path)
        LazyList(prevPaths :+ {
          if (start == adj.n) {
            EmptyPath(holePos = adj.n)
          } else {
            val (_, cost, pathLT, pathMT) = range.foldLeft((start, 0, lt(start), mt(start))) {
              case ((prevIdx, prevCost, prevLT, prevMT), pos) =>
                val cost = prevCost + c(prevIdx, pos)
                (pos, cost,
                  Seq(prevLT, lt(pos) - cost).min,
                  Seq(prevMT, mt(pos) - cost).max)
            }
            Path(start, (adj.n - 1).toByte, cost, pathLT, pathMT)
          }
        })
      } else {
        lazy val firstPath = Path(start, start, 0, lt(start), mt(start))
        for {
          path <- EmptyPath(holePos = start) +: range.scanLeft(firstPath) {
            case (Path(_, prevEnd, prevCost, prevLT, prevMT), pos) =>
              val cost = prevCost + c(prevEnd, pos)
              Path(
                start,
                pos,
                cost,
                Seq(prevLT, lt(pos) - cost).min,
                Seq(prevMT, mt(pos) - cost).max)
          }.filter(p => p.end < adj.n - k)
          combination <- kHoles(k - 1, (path.holePos + 1).toByte, prevPaths :+ path)
        } yield combination
      }
    }

    def kSlices(k: Int): LazyList[(Vector[Path], PathT)] = {
      kHoles(k).map(paths => {
        val init = paths.init.map {
          case Path(start, end, prevCost, prevLT, prevMT) =>
            val cost = prevCost + c(end, end + 1)
            Path(
              start,
              (end + 1).toByte,
              cost,
              Seq(prevLT, lt(end + 1) - cost).min,
              Seq(prevMT, mt(end + 1) - cost).max)
          case EmptyPath(hole) =>
            Path(hole, hole, 0, lt(hole), mt(hole))
        }
        (init, paths.last)
      })
    }

    def kSwap(nodes: Seq[TourIdx], pathsInput: Vector[PathT]): (Seq[TourIdx], Int) = {
      val hasZero = nodes.head == 0
      val (startCost, firstEndpoint) = if (hasZero) {
        (0, 0: Byte)
      } else {
        val path = pathsInput.head.asInstanceOf[Path]
        (Seq(c(0, path.start), path.mt).max + path.cost, path.end)
      }
      val paths = pathsInput.tail

      (for {
        choice <- nodes.permutations

        idx = if (hasZero) {
          val start = choice.indexOf(0)
          (start until choice.length) ++ (0 until start)
        } else {
          choice.indices
        }

        (pathCost, last, isValid) = idx.foldLeft(startCost, firstEndpoint, true) { case ((cost, prevEnd, wasValid), i) =>
          val node = choice(i)
          val costToNode = cost + c(prevEnd, node)
          val isValid = wasValid && costToNode <= lt(node)
          val costAtNode = Seq(costToNode, mt(node)).max

          paths(i) match {
            case Path(start, end, pathCost, lt, mt) =>
              val costBeforePath = costAtNode + c(node, start)
              val finalCost = Seq(costBeforePath, mt).max + pathCost
              (finalCost, end, isValid && costBeforePath <= lt)
            case EmptyPath(_) =>
              (costAtNode, node, isValid)
          }
        }
        if isValid
      } yield (choice, pathCost + c(last, 0))).minBy(_._2)
    }

    def tryKSwap(k: Int): Boolean = {
      val oldCost = length(tour, adj)
      kHoles(k).map { paths =>
        kSwap(paths.init.map(_.holePos), paths)
      }.find(_._2 < oldCost) match {
        case Some((path, _)) =>
          val prevTour = tour.clone()
          path.sorted.zipWithIndex.foreach {
            case (prev, idx) => tour(prev) = prevTour(path(idx))
          }
          if (tour(0) != 0) {
            val (after, before) = tour.span(_ != 0)
            tour = before ++ after
          }
          true
        case None => false
      }
    }

    def tryKOpt(k: Int): Boolean = {
      val oldCost = length(tour, adj)
      kSlices(k).map((paths, last) =>
        kOpt(paths, last)
      ).find(_._2 < oldCost) match {
        case Some((newPaths, _)) =>
          tour = newPaths.foldLeft(IndexedSeq.empty[Byte]) { (acc, path) =>
            acc ++ NumericRange(path.start, (path.end + 1).toByte, 1: Byte)
          }.map(tour(_)).toArray
          true
        case None => false
      }
    }

    def tryFlip(): Boolean = {
      val moves = for {
      // kSwap search already is considering k adjacent nodes, so skip that
        start <- (1 until adj.n - k).view
        end <- (start + k until adj.n).view
        if flipCost(start, end).exists(_ < 0)
      } yield (start, end)

      if (moves.nonEmpty) {
        val (start, end) = moves.head
        flip(start, end)
        true
      } else {
        false
      }
    }

    // tryF functions mutate tour, #:: is lazy
    while ((tryKOpt(k) #:: tryKSwap(k) #:: tryFlip() #:: LazyList.empty).exists(identity)) { }

    dprintln(s"localSearch end length: ${lengthWithLT(tour, adj)}, ${tour.toList}")
    tour
  }

  def quickOpt(adj: AdjMatrix, initTour: Seq[Byte]): Seq[NodeID] = localSearch(adj, initTour, k)

  def quickSearch(adj: AdjMatrix, restarts: Int = defaultRestarts): Seq[NodeID] = {
    val feasible = feasibleTSPTW(adj).get
    val remainingIndices = (NumericRange(0: Byte, adj.n, 1: Byte).toSet -- feasible.toSet).toVector
    val randomizedTours = (1 to restarts).map(_ => feasible ++ shuffle(remainingIndices))
    val insertionTour = solveTspInsertion(adj, feasible)

    (insertionTour +: randomizedTours).par
      .map(quickOpt(adj, _))
      .minBy(length(_, adj))
  }
}
