package tsp

import BitSetObject._
import algorithms.HungarianAlgorithm

import scala.annotation.tailrec
import scala.collection.immutable.NumericRange
import scala.collection.Seq
import scala.collection.parallel.CollectionConverters.ImmutableMapIsParallelizable
import scala.collection.parallel.{CollectionsHaveToParArray, ParMap}

/** General TSP and API tools
  *
  * Created by velizar on 30/11/16.
  */
object TSP {

  val dpThreshold = 19

  import Main.isBenchmarkRun

  // debug print statements
  def dprint(x: => Any) = if (isBenchmarkRun) print(x)
  def dprintln() = if (isBenchmarkRun) println()
  def dprintln(x: => Any) = if (isBenchmarkRun) println(x)

  type NodeID = Byte
  type ConstraintMap = Map[Byte, Int]

  // outputs: (length, isValid)
  def pathLengthWithLT(path: Seq[Byte], adj: AdjMatrix, startCost: Int = 0): (Int, Boolean) = {
    val (pathCost, _, isValid) = path.drop(1).foldLeft(
      Seq(startCost, adj.mt(path.head)).max, path.head, startCost <= adj.lt(path.head)) {
      case ((cost, a, prevValid), b) =>
        val newCost = Seq(cost + adj(a, b), adj.mt(b)).max
        (newCost, b, prevValid && (newCost <= adj.lt(b)))
    }
    (pathCost - startCost, isValid)
  }

  def pathLength(tour: Seq[Byte], adj: AdjMatrix): Int = pathLengthWithLT(tour, adj)._1

  def lengthWithLT(tour: Seq[Byte], adj: AdjMatrix): (Int, Boolean) = {
    val (pathCost, isValid) = pathLengthWithLT(tour, adj)
    (pathCost + adj(tour.last, tour.head), isValid)
  }

  def length(tour: Seq[Byte], adj: AdjMatrix): Int = lengthWithLT(tour, adj)._1

  def normalizeOrder(tour: Seq[Byte]): Seq[Byte] = {
    if (tour(0) == 0) {
      tour
    } else {
      val (after, before) = tour.span(_ != 0)
      before ++ after
    }
  }

  /** Insertion heuristic with time windows
    *
    * Greedy algorithm, for each node it picks the lowest-cost
    *   spot between two nodes to insert it, going from A-B to A-newNode-B
    * O(n^3), could be optimized to O(n^2) or faster
    *
    * @param start contains a valid path of all lt-constrained nodes
    */
  def solveTspInsertion(adj: AdjMatrix, start: Seq[Byte] = Seq(0)): Seq[Byte] = {
    val remainingNodes = NumericRange(1: Byte, adj.n, 1: Byte).filter(!start.contains(_))
    remainingNodes.foldLeft(start){ case (path, newNode) =>
      (for {
        pos <- path.indices
        newPath = path.take(pos + 1) ++ Seq(newNode) ++ path.drop(pos + 1)
        (newCost, isValid) = lengthWithLT(newPath, adj)
        if isValid
      } yield (newCost, newPath)).minBy(_._1)._2
    }
  }

  /** Calculates a feasible tour for a triangle inequality TSPTW
    *
    * Calculates a feasible tour with only the nodes with lessThan constraints
    * If no such tour exists, then declare infeasibility - because of triangle
    * inequality, it is not possible to make the tour shorter by adding any of the
    * unconstrained nodes; you cannot reach B from A earlier if you add an extra
    * location between them.
    *
    * The output does not include the unconstrained nodes
    */
  def feasibleTSPTW(adj: AdjMatrix): Option[Seq[Byte]] = {
    val nodes = 0.toByte +: adj.lt.keys.toSeq
    val subAdjTwoD = (for {
      node <- nodes
      row = adj.twoD(node)
      reducedRow = nodes.map{ row(_) }
    } yield reducedRow.toVector).toVector

    // maps 0-indexed nodes back to the original NodeIDs
    val idxInverse = nodes.indices.zip(nodes).toMap
    // inverse function of idxInverse
    val idxMap = nodes.zipWithIndex.toMap.withDefaultValue(-1)

    val reducedLT = adj.lt.map{ case(idx, value) => idxMap(idx).toByte -> value }
    val reducedMT = adj.mt.map{ case(idx, value) => idxMap(idx).toByte -> value }

    val subAdj = AdjMatrix.from2DVec(subAdjTwoD, reducedLT, reducedMT)
    val tour = solveTspDynamic(subAdj)
    Some(tour.map(idxInverse(_)))
  }

  // Implements the Held-Karp DP algorithm for O(2^n * n^2)
  def solveTspDynamic(adj: AdjMatrix): Seq[Byte] = {

    // first element is not connected to the last, also the last is appended to 0
    type CostPath = (Int, List[Byte])
    // Set of nodes -> collection of the optimal CostPaths,
    // ending with each node in the set, sorted by last node asc
    type SubsetSolution = ParMap[BitSet, Seq[CostPath]]

    val dummyCostPath: CostPath = (Int.MaxValue, List[Byte]())
    def smallerCost(a: CostPath, b: CostPath): CostPath = if (a._1 < b._1) a else b

    @tailrec
    def iterate(prev: SubsetSolution, iteration: Byte = 1): SubsetSolution = {
      val next: SubsetSolution = (for {
        subset <- NumericRange(1: Byte, adj.n, 1: Byte)
          .combinations(n = iteration).toParArray
          .map(_.foldLeft[BitSet](0)(_ + intToBitPos(_)))
      } yield {
        subset -> (for {
          newNode <- bitSetToQueue(subset).view
          previousSet = subset - intToBitPos(newNode)
          prevSeq = prev(previousSet).view
          minPath: CostPath = (for {
            (prevDistance, prevPath) <- prevSeq
            newCost = Seq(prevDistance + adj(prevPath.head, newNode), adj.mt(newNode)).max
            newPath = newNode +: prevPath
          } yield (newCost, newPath)).foldLeft(dummyCostPath)(smallerCost)
          if minPath != dummyCostPath
          if adj.ltValid(previousSet, minPath._1)
//          if minPath._1 <= upperBound
        } yield minPath).toVector
      }).toMap

      // base case
      if (iteration == adj.n - 1)
        next
      else
        iterate(next, (iteration + 1).toByte)
    }

    val first: SubsetSolution = Map((0: BitSet) -> Vector((0, List(0.toByte)))).withDefaultValue(Vector.empty).par
    if (adj.n <= 1) {
      NumericRange(0: Byte, adj.n, 1: Byte)
    } else {
      iterate(first).values.head match {
        case Nil => throw new IllegalArgumentException("Unsatisfiable constraints")
        case penultimate => penultimate.map {
          case (cost, revPath) => (cost + adj(revPath.head, 0), revPath)
        }.minBy(_._1)._2.reverse
      }
    }
  }

  def solveTspBranchAndBound(adj: AdjMatrix): Seq[Byte] = {
    val upperBound = calcUpperBound(adj)
    val reducedAdj = AdjMatrix.from2DVec(rootNodeReduction(adj, upperBound), adj.lt, adj.mt)
    BranchAndBound.solve(reducedAdj, upperBound)
  }

  def solveTspExactly(adj: AdjMatrix): Seq[Byte] = {
    if (adj.n <= dpThreshold)
      solveTspDynamic(adj)
    else
      solveTspBranchAndBound(adj)
  }

  def solveTspApproximately(adj: AdjMatrix): Seq[Byte] =
    LocalSearch.quickSearch(adj)

  def calcUpperBound(adj: AdjMatrix): Int =
    length(LocalSearch.quickSearch(adj, restarts = 3), adj)

  /** Removes edges which cannot possibly belong to an optimal TSP
    *
    * The removed edges are set to the upper bound
    *
    * O(n^5): n^2 entries, each takes n^3 for Hungarian Algorithm
    * TODO: remove edges which lead to infeasibility
    * TODO: increase the cost by x if an edge necessarily causes
    *       waiting at least x for an MT constraint
    *
    * It works by performing an optimistic heuristic, which is forced
    * to include the edges - guaranteeing to include all TSP routes
    * in its search space while including as few non-TSP routes as possible;
    * if the optimal in its search space is worse than the upper bound, then
    * the edge choice can be removed.
    */
  def rootNodeReduction(adj: AdjMatrix, upperBound: Int): Vector[Vector[Int]] = {
    import tsp.MinArborescence.minArborescence

    // offset by 1 so that node 0 becomes 1, 1 becomes 2, etc
    // this is done to work with minArborescence subgraph, which cannot contain 0
    val offsetAdj = Vector.fill(adj.n + 1)(upperBound) +: adj.twoD.map(upperBound +: _)
    val offsetAdjT = offsetAdj.transpose
    val offsetN = offsetAdj.size.toByte
    val allNodes: BitSet = math.pow(2, offsetN - 1).toInt - 1
    val newAdj = adj.twoD.toArray.map(_.toArray)

    val apAdj = adj.twoD.toArray.map(_.toArray.map(_.toDouble))
    for (i <- 0 until adj.n)
      apAdj(i)(i) = upperBound

    var removedCount = 0
    for (src <- 1 until offsetN) {
      for (dst <- 1 until offsetN if src != dst) {
        // forbid edges from src
        val offsetAdjNoSrc = offsetAdj.updated(src, offsetAdj(src).map(_ + upperBound))

        val arbLowBound = minArborescence(
          AdjMatrix.from2DVec(offsetAdjNoSrc),
          allNodes - intToBitPos(dst.toByte), // dst is root
          offsetAdj(dst)
        ) + offsetAdj(src)(dst) // complete the circle

        // forbid edges to dst
        val offsetAdjTNoDst = offsetAdjT.updated(dst, offsetAdj(dst).map(_ + upperBound))
        // Anti-arborescence
        val aArbLowBound = minArborescence(
          AdjMatrix.from2DVec(offsetAdjTNoDst),
          allNodes - intToBitPos(src.toByte), // src is root
          offsetAdj(src)
        ) + offsetAdj(src)(dst) // complete the circle

        apAdj(src - 1)(dst - 1) = -upperBound // force choice
        apAdj(dst - 1)(src - 1) = upperBound  // forbid choice (it always results in an invalid TSP)
        val apPath = new HungarianAlgorithm(apAdj).execute()
        val apLowBound = (apPath.indices zip apPath).map{ case (source, destination) =>
          adj(source, destination)
        }.sum

        if (Seq(apLowBound, arbLowBound, aArbLowBound).max > upperBound) {
          removedCount += 1
          newAdj(src - 1)(dst - 1) = upperBound
        }

        // restore
        apAdj(src - 1)(dst - 1) = adj(src - 1, dst - 1)
        apAdj(dst - 1)(src - 1) = adj(dst - 1, src - 1)
      }
    }
    dprintln("removed edges: " + removedCount)

    newAdj.toVector.map(_.toVector)
  }
}
