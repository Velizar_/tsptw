package tsp

import scala.math.log
import scala.collection.mutable.Queue
import TSP.NodeID

object BitSetObject {
  // Used to represent which nodes are included in the particular route (order is not kept)
  // 101101 is equivalent to List(0, 2, 3, 5) - each bit corresponds to the number in the respective position
  // Can only hold up to 31 bits, or 63 if switched to Long (no slowdown is observed).
  // If switching to Long, you also need to hunt down all .toInt uses and replace then with .toLong
  type BitSet = Int

  // Precalculated values, testing shows that accessing them to be (trivially) slower than calculating them on demand
  //  val intToBitPos: Map[Byte, Int] = (for (i <- 0 to 31) yield (i.toByte -> math.pow(2, i-1).toInt)).toMap
  //  val bitPosToInt: Map[Int, Byte] = (for (i <- 0 to 31; val j = math.pow(2, i-1).toInt) yield (j -> i.toByte)).toMap

  // Returns a BitSet with only one value, which is the input number
  def intToBitPos(x: Byte): BitSet = math.pow(2, x - 1).toInt
  // Given a BitSet with only one value, returns its value
  def bitPosToInt(x: Long): Byte = (log((x << 1).toDouble) / log(2)).toByte

  // starts counting from 1
  def bitSetToQueue(bitSet: BitSet): Queue[Byte] = {
    val q = Queue.empty[Byte]
    var acc: Long = 1
    while (acc <= bitSet) {
      if ((acc & bitSet) != 0) {
        q += bitPosToInt(acc)
      }
      acc <<= 1
    }
    q
  }

  def seqToBitSet(s: Seq[Byte]): BitSet = s.foldLeft[BitSet](0)(_ + intToBitPos(_))

  def bitSetContains(b: BitSet, x: Byte): Boolean = (b & intToBitPos(x)) != 0

  // Buggy behavior if x is not in b:
  //   make sure to call bitSetContains first
  def bitSetRemove(b: BitSet, x: Byte): BitSet = b - intToBitPos(x)

  // Make sure b is not empty
  def getFirstByte(b: BitSet): Byte = {
    var acc = 1
    var byte = acc & b
    while (byte == 0) {
      acc <<= 1
      byte = acc & b
    }

    bitPosToInt(acc)
  }
}