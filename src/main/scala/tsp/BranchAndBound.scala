package tsp

import scala.collection.View
import java.util.concurrent.PriorityBlockingQueue
import scalaz.Memo.immutableHashMapMemo
import scala.collection.parallel.CollectionConverters._

import BitSetObject._
import TSP.{NodeID, dprintln}

// Provides a pure TSP implementation (no IO) with Branch and Bound
object BranchAndBound {
  def solve(adj: AdjMatrix, upperBound: Int = Int.MaxValue): Seq[Byte] =
    new BranchAndBound(adj, upperBound).solve

  val parallelism = 2
}

/** Exact solution with branch and bound
  *
  * Explores the search space described in the dynamic
  * programming solution. It first solves the problem
  * heuristically to achieve an upper bound.
  *
  * Constraints are handled as follows:
  *
  * If a state violates a lessThan constraint, it is invalid
  *
  * If a state violates a moreThan constraint, continue by
  * having the driver wait until the constraint passes, as
  * this might still be optimal.
  */
class BranchAndBound(adj: AdjMatrix, upperBound: Int = Int.MaxValue) {
  import BranchAndBound._
  var heurCalls = 0

  type Heuristic = (BitSet, Byte, Int) => Int
  case class State(nodes: Seq[NodeID], cost: Int, heuristic: Int)

  // NOTE! node 0 is not included
  private val allNodes: BitSet = math.pow(2, adj.n - 1).toInt - 1
  private val visited = java.util.Collections.synchronizedMap(new java.util.HashMap[(BitSet, Byte), Int]())

  private val reverseAdj = AdjMatrix.from2DVec(adj.twoD.transpose)
  // The heuristic is a strict underestimate of the actual tour length to close the TSP tour
  // Calculates the arborescence between the remaining nodes
  // All TSP tours are also an arborescence, but not all arborescences are TSP tours.
  // This heuristic calculates the minimum arborescence.
  // (Uses the anti-arborescence because for some reason worked better when I tested it)
  def arborescenceHeuristic(prevSubset: BitSet, last: NodeID): Int = {
    heurCalls += 1
    val remainingNodes = allNodes - prevSubset - intToBitPos(last)
    // arborescence
//    val heuristic = MinArborescence.minArborescence(adj, remainingNodes, adj.twoD(last))
    // anti-arborescence
    val heuristic = MinArborescence.minArborescence(reverseAdj, remainingNodes, reverseAdj.twoD(0))

    // Close the loop - the arborescence doesn't have a cycle, it has one less edge than our tour at this point
    // This continues to be an arborescence, even better it narrows down the set of all arborescences by
    //   eliminating some arborescences which are not TSP tour completions.
    // Namely, it doesn't consider arborescences which have an outgoing edge from 0.
    // (For anti-arborescence, an analogous thing happens)
    val lastEdge = if (remainingNodes != 0) {
      val biggerPrevSubset = prevSubset + intToBitPos(last)
      // arborescence
//      adj.sortedIncomingEdges(0).dropWhile(x => bitSetContains(biggerPrevSubset, x._2)).head._1
      // anti-arborescence
      adj.sortedOutgoingEdges(last).dropWhile(x => bitSetContains(biggerPrevSubset, x._2)).head._1
    } else 0

    heuristic + lastEdge
  }

  // initialized at solve() to avoid memo reuse
  var memoPrecalcArb: ((BitSet, NodeID)) => Int = _

  def makeState(nodes: Seq[NodeID], prevNodes: BitSet, cost: Int, h: Heuristic): State =
    State(nodes, cost, h(prevNodes, nodes.head, cost))

  def heuristic(prevNodes: BitSet, prevHead: NodeID, prevCost: Int): Int =
    memoPrecalcArb(prevNodes, prevHead) + prevCost

  def nextStates(state: State, upperBound: Int): View[State] = {
    val prevBitSet: BitSet = seqToBitSet(state.nodes)
    val remainingNodes: BitSet = allNodes - prevBitSet
    if (state.nodes.length == adj.n) {
      Seq(makeState(0.toByte +: state.nodes, prevBitSet, state.cost + adj(state.nodes.head, 0), (_, _, c) => c)).view
    } else {
      for {
        newNode <- bitSetToQueue(remainingNodes).view
        pathCost = state.cost + adj(state.nodes.head, newNode)
        if pathCost <= upperBound
        cost = Seq(adj.mt(newNode), pathCost).max
        if adj.ltValid(prevBitSet, cost) // does it violate a LT constraint
        prevCost = visited.getOrDefault((prevBitSet, newNode), Int.MaxValue)
        if cost < prevCost // have we already seen a better path for this state
        _ = visited.put((prevBitSet, newNode), cost)
        newState = makeState(newNode +: state.nodes, prevBitSet, cost, heuristic)
        if newState.heuristic <= upperBound // is this guaranteed to be worse than optimal
      } yield newState
    }
  }

  def solve: Seq[Byte] = {
    heurCalls = 0
    memoPrecalcArb = immutableHashMapMemo[(BitSet, NodeID), Int] {
      case (prevSubset, last) => arborescenceHeuristic(prevSubset, last)
    }

    val fringe = new PriorityBlockingQueue[State](5000, Ordering.by(_.heuristic))
    var solution: Seq[NodeID] = null

    fringe.add(State(List(0.toByte), 0, 0))
    (1 to parallelism).par.foreach { _ =>
      while (solution == null) {
        if (!fringe.isEmpty) { // Might become prematurely empty due to parallelism
          val state = fringe.take()
          if (state.nodes.length == adj.n + 1) {
            solution = state.nodes.tail.reverse
          } else {
            nextStates(state, upperBound).foreach(fringe.add)
          }
        }
      }
    }
    dprintln("Fringe size: " + fringe.size)
    dprintln("Heuristic calls: " + heurCalls)
    solution
  }
}
