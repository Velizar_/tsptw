package tsp

import TSP.ConstraintMap
import BitSetObject.*

import scala.collection.immutable.NumericRange

object AdjMatrix {
  def from2DVec(input: Vector[Vector[Int]],
                // TODO: swap order for the next two arguments
                lessThanConstraints: ConstraintMap = Map.empty,
                moreThanConstraints: ConstraintMap = Map.empty) =
    new AdjMatrix(
      input.flatten,
      lessThanConstraints.withDefaultValue(Int.MaxValue),
      moreThanConstraints.withDefaultValue(0))

  def sortCostsWithIdx(s: Vector[Int], idx: Int): Vector[(Int, Byte)] =
    s.zip(NumericRange(0: Byte, s.length.toByte, 1: Byte)).filter(_._2 != idx).sortBy(_._1)
}

/** Converts 2D vector to 1D also supports some operations
  *
  * input: values is an n X n vector. Row x, column y is at values(y + n * x)
  */
class AdjMatrix(val values: Vector[Int],
                val lt: ConstraintMap,
                val mt: ConstraintMap) {
  import AdjMatrix._

  val length, n = math.sqrt(values.size).toByte
  val twoD: Vector[Vector[Int]] = (for (x <- 0 until n) yield
    (for (y <- 0 until n) yield
      this(x.toByte, y.toByte)).toVector).toVector

  // _(i)(j) = the cost of the jth shortest edge going out of the ith node, also its destination
  // the edge goes from i to output._2 and has length of output._1
  val sortedOutgoingEdges: Vector[Vector[(Int, Byte)]] = twoD.zipWithIndex.map((x, i) => sortCostsWithIdx(x, i))
  // _(i)(j) = the cost of the jth shortest edge going in to the ith node, also its source
  // the edge goes from output._2 to i and has length of output._1
  val sortedIncomingEdges: Vector[Vector[(Int, Byte)]] = twoD.transpose.zipWithIndex.map((x, i) => sortCostsWithIdx(x, i))

  val sortedLT: Vector[(Byte, Int)] = lt.toVector.sortBy(_._2)

  def apply(x: Byte, y: Byte): Int = values(y + n * x)
  def apply(x: Int, y: Int): Int = values(y + n * x)

  /** Checks if all lt constraints pass
    *
    * ltValid is called after a new node is added;
    * prevNodes does NOT include the new node, but
    * cost includes the cost to that node. (to
    * ensure that increasing the cost doesn't cause
    * that node to fail)
    *
    * Linear in k where k is the number of lt constraints < cost
    * Can be sped up to log2(k) or O(1)
    * Not a performance bottleneck currently
    */
  def ltValid(prevNodes: BitSet, cost: Int): Boolean = {
    sortedLT.view
      .takeWhile(cost > _._2).map(_._1) // = nodes with violated constraints
      .forall(bitSetContains(prevNodes, _))
  }
}
