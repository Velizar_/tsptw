package tsp

import scala.collection.immutable.NumericRange
import BitSetObject.*
import TSP.NodeID

import scala.annotation.tailrec
import scala.collection.mutable.PriorityQueue
import scala.math.Ordered.orderingToOrdered

type Edge = (Int, NodeID)
type EdgeWithDst = (Int, NodeID, NodeID)

/** Minimum spanning r-arborescence - heuristic for TSP
  *
  * Based on the paper by Gabow, H. N.; Galil, Z.; Spencer, T.; Tarjan, R. E. (1986),
  *   "Efficient algorithms for finding minimum spanning trees in undirected and directed graphs"
  * Only returns the cost of the tree, not the actual tree
  * The root is provided with the edgesFromRoot argument:
  *   only one of those edges is used then, also no
  *   incoming edges to the root are used.
  * Note that all TSP tours are a valid arborescence
  *   (excluding the last edge which closes the tour),
  *   but not all arborescences are a valid TSP tour.
  */
object MinArborescence {
  // Note: subgraph never includes 0
  def minArborescence(adj: AdjMatrix, subgraph: BitSet, edgesFromEndRoot: Seq[Int]): Int = {
    if (subgraph == 0)
      return 0

    val subgraphVec = bitSetToQueue(subgraph).toVector
    val k = subgraphVec.length
    val unionFind = new ByteArbrscUnionFind(subgraphVec.lastOption.getOrElse(-1), k.toByte)
    val s = unionFind.last + 1 // first supernode ID
    inline def inEdges = adj.sortedIncomingEdges
    // a heap of min incoming edges for each supernode
    val heaps: Array[SkewHeap] = Array.fill(k - 1)(SkewHeap.empty)
    // keeps track of the cost to insert an edge from the extra root for each supernode
    val edgesFromR: Array[Int] = edgesFromEndRoot.toArray ++ new Array((unionFind.maxSize - edgesFromEndRoot.size) max 0)
    val skipped: Array[Byte] = Array.fill(s)(0.toByte)
    var totalCost = 0

    // contains the current arborescence in progress
    var growthPath: List[NodeID] = List(subgraphVec.head)
    inline def root = growthPath.head
    inline def viableForNextEdge(node: NodeID) = bitSetContains(subgraph, node) && unionFind.find(node) != root
    var nodesOnPath: BitSet = intToBitPos(root)

    @tailrec
    def extractNextEdge(): (Int, NodeID, NodeID) = {
      val edge@(_, src, dst) = if (root < s) {
        val (cost, src) = inEdges(root)(skipped(root))
        (cost, src, root)
      } else {
        val res = heaps(root - s).min
        heaps(root - s) = heaps(root - s).tail
        res
      }
      skipped(dst) = (skipped(dst) + 1).toByte
      if (root >= s && skipped(dst) < adj.n - 1) {
        var candidateEdge = inEdges(dst)(skipped(dst))
        while (!viableForNextEdge(candidateEdge._2) && skipped(dst) < adj.n - 2) {
          skipped(dst) = (skipped(dst) + 1).toByte
          candidateEdge = inEdges(dst)(skipped(dst))
        }

        if (viableForNextEdge(candidateEdge._2))
          heaps(root - s) = heaps(root - s).insert((candidateEdge._1 - unionFind.cost(dst), candidateEdge._2, dst))
      }

      if (viableForNextEdge(src)) {
        edge
      } else {
        extractNextEdge()
      }
    }

    // each iteration, add the min-cost node connecting to the supernode under growthPath(lastIdx)
    while (!(growthPath.tail == Nil && nodesOnPath == subgraph)) {
      val (addedCost, next, _) = extractNextEdge()
      if (root >= s)
        heaps(root - s).addToAll(-addedCost)
      edgesFromR(root) -= addedCost
      totalCost += addedCost
      unionFind.inCost(root) = addedCost
      if (bitSetContains(nodesOnPath, next)) {
        // we just formed a cycle, merge it into a supernode
        val (nodesBeforeNext, parentOfNext :: newGrowthPath) = growthPath.span(unionFind.find(_) != unionFind.find(next))
        growthPath = newGrowthPath
        val nodesToMerge = parentOfNext :: nodesBeforeNext
        val newNode = unionFind.union(nodesToMerge)

        edgesFromR(newNode) = Int.MaxValue
        for (node <- nodesToMerge) {
          if (node < s) {
            if (skipped(node) < adj.n - 1) {
              val (cost, src) = inEdges(node)(skipped(node))
              heaps(newNode - s) = heaps(newNode - s) insert(cost - unionFind.cost(node), src, node)
            }
          } else {
            heaps(newNode - s) = heaps(newNode - s) merge heaps(node - s)
          }
          edgesFromR(newNode) = edgesFromR(newNode) min edgesFromR(node)
        }
        growthPath ::= newNode
      } else {
        nodesOnPath += intToBitPos(next)
        growthPath ::= next
      }
    }
    totalCost + edgesFromR(root)
  }
}

// Also keeps track of the node cost for min-arborescence
class ByteArbrscUnionFind(lastId: NodeID, n: Byte) {
  import tsp.BitSetObject._
  val maxSize: Int = lastId + n
  private val parent: Array[NodeID] = NumericRange(0.toByte, maxSize.toByte, 1.toByte).toArray
  val inCost: Array[Int] = new Array(maxSize)
  var last: NodeID = lastId

  // returns the parent and cost of x and applies path compression along the way
  def traverse(x: NodeID): (NodeID, Int) = {
    val xParent = parent(x)
    if (xParent == x) {
      (x, inCost(x))
    } else {
      val (topAncestor, parentCost) = traverse(xParent)
      if (xParent != topAncestor) {
        parent(x) = topAncestor
        inCost(x) = inCost(x) + parentCost - inCost(topAncestor)
      }
      (topAncestor, inCost(x) + inCost(topAncestor))
    }
  }

  def find(x: NodeID): NodeID = traverse(x)._1
  def cost(x: NodeID): Int = traverse(x)._2

  def addNode(): NodeID = {
    last = (last + 1).toByte
    parent(last) = last
    last
  }

  def union(cycle: Seq[NodeID]): NodeID = {
    val newNodeID = addNode()
    cycle.foreach { node =>
      parent(node) = newNodeID
    }
    newNodeID
  }
}

// Mutable data structure whose only mutating operation is addToAll
// Can be made generic instead of specific to Edge, but then addToAll will need extra work
object SkewHeap {
  def empty(using ord: Ordering[EdgeWithDst]): SkewHeap = EmptySkewHeap()
  def apply(elem: EdgeWithDst): SkewHeapNode = SkewHeapNode(elem, empty, empty)
}
sealed trait SkewHeap {
  def min: EdgeWithDst
  def tail: SkewHeap
  def insert(elem: EdgeWithDst): SkewHeapNode
  def merge(that: SkewHeap): SkewHeap
  def addToAll(value: Int): Unit
}
case class SkewHeapNode(var head: EdgeWithDst, var left: SkewHeap, var right: SkewHeap)(using ord: Ordering[EdgeWithDst]) extends SkewHeap {
  var addedValue = 0
  inline override def min: EdgeWithDst = {
    propagate()
    head
  }
  override def insert(elem: EdgeWithDst): SkewHeapNode = SkewHeap(elem).merge(this)
  override def tail: SkewHeap = {
    propagate()
    left.merge(right)
  }
  override def merge(that: SkewHeap): SkewHeapNode = that match {
    case EmptySkewHeap() => this
    case that@SkewHeapNode(_, thatLeft, thatRight) =>
      propagate()
      that.propagate()
      if (ord.lt(min, that.min)) {
        SkewHeapNode(min, that.merge(right), left)
      } else {
        SkewHeapNode(that.min, this.merge(thatRight), thatLeft)
      }
  }
  override def addToAll(value: Int): Unit = {
    addedValue += value
  }
  private def propagate(): Unit = {
    if (addedValue != 0) {
      head = head.copy(_1 = head._1 + addedValue)
      left.addToAll(addedValue)
      right.addToAll(addedValue)
      addedValue = 0
    }
  }
}
case class EmptySkewHeap()(using ord: Ordering[EdgeWithDst]) extends SkewHeap {
  override def min: EdgeWithDst = throw new NoSuchElementException("empty.min")
  override def tail: SkewHeap = throw new NoSuchElementException("empty.tail")
  override def insert(elem: EdgeWithDst): SkewHeapNode = SkewHeap(elem)
  override def merge(that: SkewHeap): SkewHeap = that
  override def addToAll(value: Int): Unit = { }
}
