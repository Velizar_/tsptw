package tsp

import scala.collection.Seq
import scalaz.syntax.std.boolean._
import TSP.{ConstraintMap, solveTspExactly, solveTspApproximately}

object Main {
  var isBenchmarkRun: Boolean = false

  def main(args: Array[String]): Unit = {
    isBenchmarkRun = args.toList == List("benchmark")
    if (!isBenchmarkRun && args.length < 2) {
      println("Incorrect input. Please see README.md for the input format.")
    } else if (isBenchmarkRun) {
      Benchmark.run()
    } else {
      print(runFromCL(args))
    }
  }

  def solve(adj: AdjMatrix, isExact: Boolean): Seq[Byte] = {
    if (isExact || adj.n < LocalSearch.minSize)
      solveTspExactly(adj)
    else
      solveTspApproximately(adj)
  }

  def cliArgsToAdjMatrix(cliArgs: Vector[String]): AdjMatrix = {
    val adjVec: Vector[Vector[Int]] = cliArgs.map(node => {
      node.split("/")(0).split(",").map(_.toInt).toVector // = a row of distances from _node_
    })
    val (lessThanConstraints, moreThanConstraints) = parseConstraints(cliArgs)
    AdjMatrix.from2DVec(adjVec, lessThanConstraints, moreThanConstraints)
  }

  def runFromCL(args: Array[String]): String = {
    val solveType +: matrixArgs = args.toVector
    val adj = cliArgsToAdjMatrix(matrixArgs)
    try {
      val path = solve(adj, solveType == "exact")
      path.mkString(",")
    } catch {
      case e: IllegalArgumentException if e.getMessage == "Unsatisfiable constraints" => "Error: unsatisfiable constraints"
    }
  }

  def parseConstraint(input: String, i: Byte): (Option[(Byte, Int)], Option[(Byte, Int)]) = {
    val Array(mt, lt) = input.split(",")

    val ltConstraint = !lt.equals("-") option (i, lt.toInt)
    val mtConstraint = !mt.equals("-") option (i, mt.toInt)

    (ltConstraint, mtConstraint)
  }

  def parseConstraints(args: Seq[String]): (ConstraintMap, ConstraintMap) = {
    val (ltConstraints, mtConstraints) = args.zipWithIndex.map{ case (str, node) =>
      val split = str.split("/")
      parseConstraint(split(1), node.toByte)
    }.unzip

    (ltConstraints.flatten.toMap,
     mtConstraints.flatten.toMap)
  }
}