An optimizer for the aTSPTW problem - asymmetric travelling salesman problem with time windows. The time windows refer to the value of the objective function.

For example, the ironing project needs to minimize the time spent on a route which covers n customers, provides the distances between each pair of customers, and some customers may need to be visited in a certain time window (an example of a time window is "after 20:00", or "between 19:00 and 22:00").

Table of Contents
=================

   * [Usage](#usage)
      * [Input format](#markdown-header-input-format)
      * [Output format](#markdown-header-output-format)
   * [Compilation](#markdown-header-compilation)
   * [Development environment](#markdown-header-development-environment)
   * [Notable files](#markdown-header-notable-files)
   * [Main components](#markdown-header-main-components)
      * [Solvers](#markdown-header-solvers)
         * [BranchAndBound.solve](#markdown-header-branchandboundsolve)
            * [Input](#markdown-header-input)
            * [Output](#markdown-header-output)
            * [Solution quality](#markdown-header-solution-quality)
            * [Performance](#markdown-header-performance)
         * [TSP.solveTspDynamic](#markdown-header-tspsolvetspdynamic)
            * [Input](#markdown-header-input-1)
            * [Output](#markdown-header-output-1)
            * [Solution quality](#markdown-header-solution-quality-1)
            * [Performance](#markdown-header-performance-1)
         * [LocalSearch.localSearch](#markdown-header-localsearchlocalsearch)
            * [Input](#markdown-header-input-2)
            * [Output](#markdown-header-output-2)
            * [Solution quality](#markdown-header-solution-quality-2)
            * [Performance](#markdown-header-performance-2)
         * [TSP.solveTspInsertion](#markdown-header-tspsolvetspinsertion)
            * [Input](#markdown-header-input-3)
            * [Output](#markdown-header-output-3)
            * [Solution quality](#markdown-header-solution-quality-3)
            * [Performance](#markdown-header-performance-3)
      * [Lower bounds](#markdown-header-lower-bounds)
         * [MinArborescence.minArborescence](#markdown-header-minarborescenceminarborescence)
            * [Input](#markdown-header-input-4)
            * [Output](#markdown-header-output-4)
            * [Performance](#markdown-header-performance-4)
         * [MinArborescence.minArborescence [with different arguments]](#markdown-header-minarborescenceminarborescence-with-different-arguments)
            * [Description](#markdown-header-description)
            * [Input](#markdown-header-input-5)
            * [Output](#markdown-header-output-5)
            * [Performance](#markdown-header-performance-5)
         * [new HungarianAlgorithm(arg).execute()](#markdown-header-new-hungarianalgorithmargexecute)
            * [Input](#markdown-header-input-6)
            * [Output](#markdown-header-output-6)
            * [Performance](#markdown-header-performance-6)
      * [Other](#markdown-header-other)
         * [TSP.rootNodeReduction](#markdown-header-tsprootnodereduction)
            * [Input](#markdown-header-input-7)
            * [Output](#markdown-header-output-7)
            * [Solution quality](#markdown-header-solution-quality-4)
            * [Performance](#markdown-header-performance-7)
         * [TSP.feasibleTSPTW](#markdown-header-tspfeasibletsptw)
            * [Input](#markdown-header-input-8)
            * [Output](#markdown-header-output-8)
            * [Solution quality](#markdown-header-solution-quality-5)
            * [Performance](#markdown-header-performance-8)
   * [Implementation](#markdown-header-implementation)
      * [Main](#markdown-header-main)
      * [TSP.solveTspDynamic](#markdown-header-tspsolvetspdynamic-1)
      * [TSP.feasibleTSPTW](#markdown-header-tspfeasibletsptw-1)
      * [TSP.solveTspInsertion](#markdown-header-tspsolvetspinsertion-1)
      * [LocalSearch.localSearch](#markdown-header-localsearchlocalsearch-1)
      * [MinArborescence.minArborescence](#markdown-header-minarborescenceminarborescence-1)
      * [TSP.rootNodeReduction](#markdown-header-tsprootnodereduction-1)
      * [BranchAndBound.solve](#markdown-header-branchandboundsolve-1)

# Usage

## Input format
Example:  
exact 0,820,681/-,- 907,0,810/1000,- 926,775,0/-,700

Format (for 3 addresses): exact|approximate adj_from_1 adj_from_2 adj_from_3  
Format for adj_from_i: outgoing_distances/time_windows  
Format for outgoing_distances - each distance is an integer: dist_to_1,dist_to_2,dist_to_3  
Example: 0,748,823    
Format for time_windows: lower_bound|-,upper_bound|-  
Example: 876,-  
Time windows are optional integers. A missing time window is denoted as `-`. Both can be present.  
Note: in rare cases, our program might appear to violate a lower bound. What actually happens is that
it decided that it's most efficient to stop and waste time right before visiting that address.

Detailed example:  
exact 0,748,823,1609/-,- 1048,0,436,2045/1200,- 1123,436,0,1964/-,- 1909,2045,1964,0/3200,5500  
Here, the distance from the first address to the second address is 748  
Also, we must have used between 3500 and 5500 units of distance by the time we reach the 4rd address

The optimal route is 0,2,1,3  
We always start from 0. In this case, we visit 2, then 1, then 3, then return to 0  
By the time we visit 1, we have visited edges 0->2 + 2->1 = 823 + 436 = 1259. This satisfies its lower time bound of 1200  
By the time we visit 3, we have spent 3304 time, which fits within its time window of 3200,5500  
The total length of this route is 5213

The maximum size of the input is 32 rows (n=32).  
This can be improved to up to 64 as follows:  
In the file BitSet.scala change the line 'type BitSet = Int' to 'type BitSet = Long'.

The number of LT-constrained nodes should be <= 19.  
Otherwise, finding an initial feasible tour might take too long and too much RAM, several GB if they are >= 24.  
One workaround is to solve with branch and bound without providing an upper bound.  
That will make little difference to the runtime, but will take a bit more RAM.

## Output format
A comma-separated list which is a permutation of the numbers from 0 to n-1  
Each number refers to one of the addresses in the input, following the order which the input uses.
If the input has unsatisfiable LT constraints then the output will be "Error: unsatisfiable constraints"

Example:  
For this input: `exact 0,748,823,1609/-,- 1048,0,436,2045/1200,- 1123,436,0,1964/-,- 1909,2045,1964,0/3200,5500`  
The optimal solution gives the output `0,2,1,3`  
We always start from 0. In this case, we visit 2, then 1, then 3, then return to 0  
By the time we visit 1, we have visited edges 0->2 + 2->1 = 823 + 436 = 1259. This satisfies its lower bound of 1200  
By the time we visit 3, we have spent 3304 time, which fits within its time window of 3200,5500  
The total length of this route is 5213

# Compilation
Run `sbt assembly` and look in target/scala-3.1.3/TSPSolver.jar

# Notable files
**Main.scala** - entry point, parses the input and passes it to TSP.scala.

**TSP.scala** - general TSP tools too small to have their own file.

**BitSet.scala**:  
  Tools for treating an Int or Long as a bitset, which requires much less storage than collection.[mutable or immutable].BitSet.  
  Used to represent an unordered subset of all nodes.

**AdjMatrix.scala** - adjacency matrix, but also with all of the time window constraints.

**Unused.scala** - nontrivial failed attempts which didn't perform well in practice, or need improvements to be viable; not referenced by the rest of the codebase.

# Main components
This section will describe the inputs and outputs of the most notable functions.

## Solvers

### BranchAndBound.solve

#### Input
AdjMatrix and an optional upper bound of the optimal solution for decreased RAM usage
#### Output
The optimal tour
#### Solution quality
Optimal
#### Performance
Exponential in n in practice, no theoretical bounds available

### TSP.solveTspDynamic

#### Input
AdjMatrix
#### Output
The optimal tour
#### Solution quality
Optimal
#### Performance
Better than BranchAndBound for n <= 19;  
Very consistent and determined by n (unaffected by time windows),
  as it solves with a form of a brute force with O(n^2 * 2^n).

### LocalSearch.localSearch

#### Input
AdjMatrix, initial feasible tour
#### Output
A feasible high-quality tour, deterministic function of the inputs
#### Solution quality
Almost always optimal or near-optimal, but no guarantees of solution quality,
  except that it's at least as good as the initial feasible tour.
#### Performance
Comparable to BranchAndBound, but faster, using less RAM, and more scalable with problem size and time windows;  
No performance worst case guarantees; will always compute at least n ^ k (in practice, k = 4).

### TSP.solveTspInsertion

#### Input
AdjMatrix, initial feasible tour containing all nodes with LT constraints
#### Output
A feasible low-quality tour, deterministic function of the inputs
#### Solution quality
Low quality, not good enough to be used as end output (often > 20 minutes longer than the optimal)
#### Performance
O(n^3), can be improved to O(n^2) or better

## Lower bounds

### MinArborescence.minArborescence

#### Input
Sorted incoming edges for each node,  
A subgraph as BitSet (nodes outside of the subgraph will be ignored),  
Sorted outgoing edges for the root node,  
The ID of the root node,  
Optionally the first node of the arborescence.
#### Output
The cost of the minimum-cost arborescence of the subgraph, which serves as
  a lower bound to the aTSP starting from the root node and including the subgraph.  
(Note: an aTSP lower bound is also an aTSPTW lower bound, regardless of the time windows).
#### Performance
O(k*n)

### MinArborescence.minArborescence [with different arguments]

#### Description
This alternative input results in anti-arborescence, where the root is the end node.
In the regular arborescence, you can reach every node from a root;
In the anti-arborescence, you can reach a root from every node.
#### Input
Sorted outgoing edges for each node,  
a subgraph as BitSet (nodes outside of the subgraph will be ignored) of size k,  
sorted incoming edges for the root node,  
the ID of the root node,  
optionally the first node of the anti-arborescence.
#### Output
The cost of the minimum-cost anti-arborescence of the subgraph.  
This might provide a tighter or a less tight bound than the min arborescence, everything else still applies.
#### Performance
O(k*n)

### new HungarianAlgorithm(arg).execute()

#### Input
Adjacency matrix as Array[Array[Double]]
#### Output
The solution of the assignment problem for the graph;  
Its cost is a lower bound to the aTSP.  
(Note: an aTSP lower bound is also an aTSPTW lower bound regardless of the time windows)
#### Performance
O(n^3), but tends to be very fast in practice

## Other

### TSP.rootNodeReduction

#### Input
AdjMatrix and an upper bound on the optimal solution
#### Output
Eliminates edges which can't be in the optimal solution and
  returns a matrix with those edges set to a very high cost,
  effectively reducing the search space for any optimal solver.
#### Solution quality
No guarantees, can be as low as 0 removed edges even with the tightest upper bound and no time windows
#### Performance
O(n^5), 400 ms for n = 25

### TSP.feasibleTSPTW

#### Input
AdjMatrix
#### Output
A feasible tour of the node 0 followed by all the LT-constrained nodes.  
Any permutation of the remaining nodes appended to this output is feasible.
#### Solution quality
Optimal (Note: that is unlikely to be very useful)
#### Performance
O(k^2 * 2^k), where k is the number of LT-constrained nodes in the graph

# Implementation

![alt tag](http://i.imgur.com/m9G0H2A.png)
Component dependency diagram

## Main
- With **exact, n <= 19**, the tour is optimized just with TSP.solveTspDynamic.
- The **approximate** solution uses LocalSearch.localSearch with 10 runs,
  the first of which uses as an initial tour the TSP.solveTspInsertion output for some guarantee of quality,
  while the rest all have TSP.feasibleTSPTW followed by a random permutation of the remaining nodes.
- With **exact, n > 19**, first an upper bound is obtained from the approximate solution but with 4 runs (without the last 6),
  then root node reduction is used along with the upper bound,
  and finally branch and bound is called with the reduced AdjMatrix and the same upper bound.

## TSP.solveTspDynamic
The Held-Karp exact DP algorithm for aTSP, but with time windows added.

Whenever a new state is reached, it is checked for constraints:

- If an LT constraint is violated or becomes unsatisfiable, it is pruned;
- If an MT constraint is violated, the cost is increased until the MT constraint is no longer violated;
- Every previous pruned state is not considered, and if all states are pruned, then the follow-up state is
  also pruned - this is functionally equivalent to setting a pruned state's cost to a very high value.

## TSP.feasibleTSPTW
Runs TSP.solveTspDynamic to the LT-constrained nodes.

If it cannot find a solution then there is no solution because:  
Including extra nodes can only increase the path length because of the triangle inequality;  
Increasing the cost at one node always increases the cost at the following nodes with the same amount;  
Every LT constraint which was violated will still be violated because its node cost only went up or stayed the same.
  
## TSP.solveTspInsertion
Based on the insertion heuristic for aTSP which inserts each node in the position which causes the path cost to increase minimally.

Starts from the output of TSP.feasibleTSPTW to guarantee that it can find a feasible solution.  
All LT-constrained nodes are already in place.  
The following modifications are made to the original algorithm:
  1. An insertion cannot violate an LT constraint [this can always be satisfied - proof: one way to guarantee is by always inserting after the last node];
  2. If an MT constraint is violated, adjust the cost to include the wait time; count the adjustment in the cost when choosing where to insert [the min cost is chosen].

## LocalSearch.localSearch
Includes three searches: k-opt, k-swap, and flip (in that order); with k = 4.

Whenever a search finds an improvement, the improvement is applied and
  the search prodecure is restarted from the first search;  
Whenever a search fails to find improvements, the next search is attempted;  
If all searches fail to find improvements, the algorithm terminates;  
The search only considers the current state, which is also the min-cost
  state we have found so far during the method invocation - different neighbourhoods
  are explored only by restarting with a different initial state.

A rough description of the searches follows, then some of the details will be mentioned.

k-opt reduces the tour to k "slices" by "cutting off" k of its edges
then it looks at all possible permutations (brute force) of the ways to reattach
(roughly, each slice can follow each slice) and chooses the min-cost feasible. For example:

![alt tag](http://i.imgur.com/PLSiSET.png)

---

k-swap makes k "holes" in the tour by "taking away" k of its nodes
then it looks at all possible permutations (brute force) of the ways to replace them to fill the holes
(roughly, each node can fill each hole) and chooses the min-cost feasible. For example:

![alt tag](http://i.imgur.com/R54f91i.png)

---

flip reverses a path in the subtour, e.g. from 1-2-3-4-5-6 to 1-5-4-3-2-6. For example:

![alt tag](http://i.imgur.com/091SOI4.png)

---

k-opt and k-swap consider all combinations of the ways to choose the slices/holes.  
The nodes between two holes/slices are treated as one path with precomputed LT and MT constraint and cost.  
All handling of LT and MT is hard to code but obvious to figure out, for details see the source code.  
A special case is handled whenever one of the slices is the edge connecting the last
  node to the first node (node 0), or if one of the holes is node 0:

  - In the first case, the last node no longer needs to be connected to node 0;
  - In the second case, the acc cost in each permutation starts from node 0's new position (used for constraint calculating).

## MinArborescence.minArborescence
Calculates standard minimum spanning arborescence, the only deviations are performance optimizations.  
  Despite that, this is arguably the most difficult to understand algorithm in the project.

All relevant edges are pre-sorted, which is trivial to compute for Branch and Bound which calls minArborescence
  thousands of times, but with different subgraphs. A harder part is that any edge leading to a node outside of
  the relevant subgraph needs to be skipped, leading to 0 to k*(n-k) skips per arborescence calculation. 

It is based on a growth path, as in Gabow, H. N.; Galil, Z.; Spencer, T.; Tarjan, R. E. (1986),
  "Efficient algorithms for finding minimum spanning trees in undirected and directed graphs".

Also, it is constrained to contain only one edge from the root, because this ignores some
  arborescences which are possibly cheaper than the min TSP tour at a low computational cost.
  The technique for that is inspired by the same paper, and can be applied to one node only
  (the root node was an arbitrary choice).

A modified union find with path compression and union by rank is used to represent the supernodes.
  It keeps track of each node's supernode, but also each node's "supernode cost", which is explained later.

---
The grow steps start with an arbitrary node treated as a single-node supernode, which becomes the only item in the
  growth path, and also the path root. At each step, the minimum incoming edge to the path root is found - this
  process will be explained later, but for a single-node supernode, that evaluates to just the **minimum incoming edge**.  

  If the new node is not on the growth path, it is connected to the root node and becomes the new root.  

  Else, adding the edge from the new node to the current root node creates a cycle consisting of all nodes between
    them on the growth path; every supernode on that cycle is "contracted" into a single supernode.  

  After that, the growth step is repeated until there is only one remaining supernode on the growth path. At that point,
    the supernode is connected to the arborescence root (explained later), and the cost is calculated (also explained later).

![alt tag](http://i.imgur.com/TFDbmmb.png)

Near the start of the algorithm, before any contraction has happened, all supernodes have only 1 node each.
  Every next node on the growth path eventually gets an incoming and an outgoing edge. When the root node's
  next incoming edge is on the root path, that forms a cycle and it is contracted into one supernode, which
  is then treated as a single element on the growth path, and can then in turn be contracted at a later step
  into a supernode, which is how a supernode's elements can be arbitrarily nested (also, a node can be in
  an arbitrary depth of supernodes). Every inner supernode (e.g. supernode with a parent) has also an
  external incoming edge, and ultimately each supernode needs to become a spanning arborescence which starts
  from the external incoming edge, so it will not need the edge to the supernode which is reached from
  the external edge. But we still need to remember all the internal edges which make a cycle, for
  reasons explained below.

---

In order to find the minimum incoming edge to a supernode, it must also be taken into account that this edge will replace the other
  incoming edge to its destination node, because that node will become the starting node. With the nested nature of a supernode,
  this will happen on several levels - in particular, on all parent supernodes of the node. For example if A is the root node,
  B is a supernode inside A, and C is a one-node supernode inside B, then connecting an external incoming node to C will mean that we can
  remove the edge within A which leads to B, since we start from B and can reach every remaining node in A because it is a cycle;
  we can also remove the edge to C within B, because we can then reach every other node in B and continue to B's outgoing edge (which
  leads to the remaining nodes of A). Removing B's incoming edge means we must also restore its inner edge so that B forms a cycle.
  Everything said for B applies to any levels below that (e.g. if C were a supernode with many supernodes), only A already formed a cycle.

![alt tag](http://i.imgur.com/M5shgxJ.png)

The cost of including an incoming edge to a node equals the edge cost minus the cost of the edges which will be removed plus the
  cost of the edges which will have to be re-added in order to restore the cycles. But for each external incoming edge, we only need to
  store its cost minus the cost of the edge which it replaces, because removing it always goes together with re-adding the internal edge.
  So if we store that difference value for each supernode, then calculating the cost of including an incoming edge to that supernode
  becomes just the sum of those differences - for the supernode's previous incoming edge plus its parent's previous incoming edge, etc.
  The new formula then is the cost of the new edge minus the incoming edge it replaces minus all the differences (i.e. of the current
  supernode, its parent supernode, its parent's parent, etc). But there is still some redundancy - all supernodes in the same supernode
  share a parent, and therefore share that sum - which is exactly what path compression solves. This matches exactly the find operation,
  which can find the top-level parent of each supernode, so whenever either find() or cost() is performed, path compression is applied for both
  of those values. This destroys the internal structure, which means that the full arborescence cannot be recovered, nor its cost, which
  is why the cost difference (a.k.a. added cost) of each new edge is added to a variable, so that we can restore the cost. If the
  arborescence needs to also be restored, then in addition to the cost, one way to achieve it would be to store which edges and neg-edges
  are stored with each new edge, cancel out each neg-edge with the corresponding edge, and take the remaining edges.

---

After all the nodes in the subgraph are included and contracted into one supernode, the arborescence root (not to be confused with
  growth path root) is taken into account by including the edge from it which will add the minimum cost, as per the definition above.
  This forms a min-cost spanning arborescence with a 1-edge restriction to the root. Adding the root edge as the last step, and adding
  only one edge from the root, is equivalent to adding a huge number to all the outgoing edges from the root, which was suggested
  in the paper mentioned earlier. This is because if all edges from the root node had a huge cost, then one edge will only be taken
  after every other possible edge is taken (which forms supernode from all the nodes of the subgraph), which will complete the arborescence.

---

Meta note: this arborescence documentation lacks proof of correctness and performance, and could be rewritten to be more clear.

## TSP.rootNodeReduction
Tries to take edge e for every e, and checks if the problem is still solvable for a value below the upper bound.

The check is done by calculating three lower bounds and taking the tightest:
  minimum spanning arborescence, minimum spanning anti-arborescence, assignment problem.  
Each lower bound has its own different way to enforce that edge e must be included.  
If the lower bound cost is higher than the upper bound of the optimal solution, then the edge is removed.

## BranchAndBound.solve
aTSPTW Branch and bound solver

Each state forms a connected path starting from node 0, finishing at node l, and including only a subset of all nodes.

Branches on each of the possible choices of next edge, which is all the edges from l to a node outside of the current path.

All the follow-up states are filtered by the following three filters before they're put in the fringe with a heuristic:

- Inspired from the Held-Karp DP algorithm, the cost of each (includedNodes: BitSet, lastNode: Byte) is stored or if there was already
  a lower such cost, then the state is pruned. This doesn't give an upper bound of O(n^2 * 2^n) explored states
  because the order of state exploration depends on the heuristic, e.g. it's possible to spend a while exploring a state deeply, and
  only after that to find a state which has the same (previousNodes, lastNode), but with a lower cost.
- A state is also pruned if an LT constraint becomes violated or unsatisfiable.
- A state is also pruned if its cost is above the upper bound. This also happens if it contains an edge which
  was eliminated by TSP.rootNodeReduction.

The heuristic is MinArborescence.minArborescence, the root is the last node on the path, the allowed subset is the subset of
  the remaining nodes, and the distance from the smallest node in the allowed subset to the 0 node is added. This
  gives a lower bound on a TSP path from the last node to the node 0 including all the remaining nodes ("allowed subset").

On branching with an MT-constrained node as the new last node, the MT constraint is also taken into account by:
```
cost = max(mt(lastNode), cost)
```
This and the pruning from the LT constraints ensure that the time windows will be satisfied without giving up optimality.

As usual for branch and bound, all states are kept in a fringe (priority queue), and the one with a min-cost heuristic
  is chosen for branching; if it is also a final state - in this case, a full TSP route - then it is returned as
  the optimal result.

This algorithm used to be parallelized, and can be parallelized again with good success by replacing the shared global
  variable of the byte arborescence union find (initialized with MinArborescence.init(adj.n)) with either thread-global,
  or initializing a new one for each minArborescence call; everything else is thread-safe. The potential for parallelism
  is quite good because the bottleneck by far is the minArborescence call; because of the shared heap, some threads might
  branch on states which are not the minimum in the heap, even if otherwise the new minimum was going to be generated
  from the current minimum, and those states were never going to be explored, they are also likely to lead to the optimum,
  also the observed speedup is very good.

In practice, when constrained to 320 MB RAM it might fail with examples as small as n = 25, and possibly smaller, especially
  with a variety of tour-defining time windows, as the heuristic ignores time windows. There are many avenues to improvement
  (detailed in a document not included here).
