ThisBuild / scalaVersion := "3.1.3"

lazy val root = (project in file("."))
  .settings(
    name := "tsptw",
    libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.3.7",
    libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % Test,
    assembly / mainClass := Some("tsp.Main"),
    assembly / assemblyJarName := "TSPSolver.jar"
  )
